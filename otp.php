<!-- ==== Header === -->
<?php include('common/header.php') ?>

<section class="login_section top-space el">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mx-auto">
                <div class="login_wrap">
                    <div class="login_section_area">
                        <div class="header_area">
                            <h1>Verify your phone</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            </p>
                            <div class="header_image1">
                                <img src="images/vector1.png" alt="..." />
                            </div>
                        </div>
                        <div class="box_area otp">
                            <form>
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="user_name"> OTP</label>
                                            <p>
                                                OTP sent successfully
                                            </p>
                                            <div class="passcode-wrapper">
                                                <input id="digit-1" name="digit-1" type="number" maxlength="1"
                                                    onkeyup="onKeyUpEvent(1, event)" onfocus="onFocusEvent(1)">
                                                <input id="digit-2" name="digit-2" type="number" maxlength="1"
                                                    onkeyup="onKeyUpEvent(2, event)" onfocus="onFocusEvent(2)">
                                                <input id="digit-3" name="digit-3" type="number" maxlength="1"
                                                    onkeyup="onKeyUpEvent(3, event)" onfocus="onFocusEvent(3)">
                                                <input id="digit-4" name="digit-4" type="number" maxlength="1"
                                                    onkeyup="onKeyUpEvent(4, event)" onfocus="onFocusEvent(4)">
                                                <input id="digit-5" name="digit-5" type="number" maxlength="1"
                                                    onkeyup="onKeyUpEvent(5, event)" onfocus="onFocusEvent(5)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="login_button">
                                            <a href="javascript:;" class="btn btn-primary">Send Link</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="image_4">
        <img src="images/vector2.png" alt="..." />
    </div>
    <div class="image_5">
        <img src="images/vector3.png" alt="..." />
    </div>
</section>





<!-- ==== footer === -->
<?php include('common/footer.php') ?>
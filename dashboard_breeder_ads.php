<!-- ==== Header === -->
<?php include('common/header.php') ?>
<!-- ==== Header === -->
<?php include('common/header2.php') ?>

<section class="dashboard_section">
    <div class="dashboard_inner top-space">
        <?php include('dashboard_sidebar2.php') ?>
        <div class="right_side_wrap">
            <div class="setting">
                <div class="breadcame margin">
                    <div class="breadcame_area">
                        <h2>My Ads</h2>
                    </div> 
                </div>
                <div class="my_ads_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="tab_area">
                                    <div class="left_in">
                                        <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link active" id="pills-all-tab" data-bs-toggle="pill" data-bs-target="#pills-all" type="button" role="tab" aria-controls="pills-all" aria-selected="true">All</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-dog-ads-tab" data-bs-toggle="pill" data-bs-target="#pills-dogs-ads" type="button" role="tab" aria-controls="pills-dogs-ads" aria-selected="false">Dogs ads</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-ucl-ads-tab" data-bs-toggle="pill" data-bs-target="#pills-ucl-ads" type="button" role="tab" aria-controls="pills-ucl-ads" aria-selected="false">UCL ads</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-sold-tab" data-bs-toggle="pill" data-bs-target="#pills-sold" type="button" role="tab" aria-controls="pills-sold" aria-selected="false">Sold</button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
                                        <div class="content_area">
                                            <div class="row">
                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="ad_area">
                                                        <div class="inner_area">
                                                            <div class="left_area">
                                                                <img src="images/dog.png" alt="..." />
                                                                <div class="flag">
                                                                    <img src="images/flag2.png" alt="..." />
                                                                </div>
                                                                <div class="heart">
                                                                    <a href="javascript:;">
                                                                        <i class="far fa-heart"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="right_area">
                                                                <div class="content">
                                                                    <div class="left">
                                                                        <p>Golden Retriever<i class="far fa-venus ps-3"></i></p>
                                                                        <h4>£800.00</h4>
                                                                        <h5>Dublin, Ireland</h5>
                                                                        <h6>Lorem ipsum dolor et, Adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. </h6>
                                                                        <div class="button_area">
                                                                            <ul>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Bump ad</a></li>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Highlight ad</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right">
                                                                        <div class="icon_area">
                                                                            <ul>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-pen"></i></a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-trash-alt"></i></a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="corner_title change_bg1">
                                                            <p>On Hold</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_area">
                                            <div class="row">
                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="ad_area">
                                                        <div class="inner_area">
                                                            <div class="left_area">
                                                                <img src="images/listdog4.png" alt="..." />
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="heart">
                                                                    <a href="javascript:;">
                                                                        <i class="far fa-heart"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="right_area">
                                                                <div class="content">
                                                                    <div class="left">
                                                                        <p>Golden Retriever<i class="far fa-venus ps-3"></i></p>
                                                                        <h4>£800.00</h4>
                                                                        <h5>Dublin, Ireland</h5>
                                                                        <h6>Lorem ipsum dolor et, Adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. </h6>
                                                                        <div class="button_area">
                                                                            <ul>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Bump ad</a></li>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Highlight ad</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right">
                                                                        <div class="icon_area">
                                                                            <ul>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-pen"></i></a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-trash-alt"></i></a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="corner_title change_bg2">
                                                            <p>Avaliable</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_area">
                                            <div class="row">
                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="ad_area">
                                                        <div class="inner_area">
                                                            <div class="left_area">
                                                                <img src="images/dog3.png" alt="..." />
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="heart">
                                                                    <a href="javascript:;">
                                                                        <i class="far fa-heart"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="right_area">
                                                                <div class="content">
                                                                    <div class="left">
                                                                        <p>Golden Retriever<i class="far fa-venus ps-3"></i></p>
                                                                        <h4>£800.00</h4>
                                                                        <h5>Dublin, Ireland</h5>
                                                                        <h6>Lorem ipsum dolor et, Adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. </h6>
                                                                        <div class="button_area">
                                                                            <ul>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Bump ad</a></li>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Highlight ad</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right">
                                                                        <div class="icon_area">
                                                                            <ul>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-pen"></i></a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-trash-alt"></i></a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="corner_title change_bg2">
                                                            <p>Avaliable</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-dogs-ads" role="tabpanel" aria-labelledby="pills-dogs-ads-tab">
                                        <div class="content_area">
                                            <div class="row">
                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="ad_area">
                                                        <div class="inner_area">
                                                            <div class="left_area">
                                                                <img src="images/dog.png" alt="..." />
                                                                <div class="flag">
                                                                    <img src="images/flag2.png" alt="..." />
                                                                </div>
                                                                <div class="heart">
                                                                    <a href="javascript:;">
                                                                        <i class="far fa-heart"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="right_area">
                                                                <div class="content">
                                                                    <div class="left">
                                                                        <p>Golden Retriever<i class="far fa-venus ps-3"></i></p>
                                                                        <h4>£800.00</h4>
                                                                        <h5>Dublin, Ireland</h5>
                                                                        <h6>Lorem ipsum dolor et, Adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. </h6>
                                                                        <div class="button_area">
                                                                            <ul>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Bump ad</a></li>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Highlight ad</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right">
                                                                        <div class="icon_area">
                                                                            <ul>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-pen"></i></a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-trash-alt"></i></a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="corner_title change_bg1">
                                                            <p>On Hold</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_area">
                                            <div class="row">
                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="ad_area">
                                                        <div class="inner_area">
                                                            <div class="left_area">
                                                                <img src="images/listdog4.png" alt="..." />
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="heart">
                                                                    <a href="javascript:;">
                                                                        <i class="far fa-heart"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="right_area">
                                                                <div class="content">
                                                                    <div class="left">
                                                                        <p>Golden Retriever<i class="far fa-venus ps-3"></i></p>
                                                                        <h4>£800.00</h4>
                                                                        <h5>Dublin, Ireland</h5>
                                                                        <h6>Lorem ipsum dolor et, Adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. </h6>
                                                                        <div class="button_area">
                                                                            <ul>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Bump ad</a></li>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Highlight ad</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right">
                                                                        <div class="icon_area">
                                                                            <ul>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-pen"></i></a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-trash-alt"></i></a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="corner_title change_bg2">
                                                            <p>Avaliable</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_area">
                                            <div class="row">
                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="ad_area">
                                                        <div class="inner_area">
                                                            <div class="left_area">
                                                                <img src="images/dog3.png" alt="..." />
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="heart">
                                                                    <a href="javascript:;">
                                                                        <i class="far fa-heart"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="right_area">
                                                                <div class="content">
                                                                    <div class="left">
                                                                        <p>Golden Retriever<i class="far fa-venus ps-3"></i></p>
                                                                        <h4>£800.00</h4>
                                                                        <h5>Dublin, Ireland</h5>
                                                                        <h6>Lorem ipsum dolor et, Adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. </h6>
                                                                        <div class="button_area">
                                                                            <ul>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Bump ad</a></li>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Highlight ad</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right">
                                                                        <div class="icon_area">
                                                                            <ul>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-pen"></i></a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-trash-alt"></i></a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="corner_title change_bg2">
                                                            <p>Avaliable</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-ucl-ads" role="tabpanel" aria-labelledby="pills-ucl-ads-tab">
                                        <div class="content_area">
                                            <div class="row">
                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="ad_area">
                                                        <div class="inner_area">
                                                            <div class="left_area">
                                                                <img src="images/dog.png" alt="..." />
                                                                <div class="flag">
                                                                    <img src="images/flag2.png" alt="..." />
                                                                </div>
                                                                <div class="heart">
                                                                    <a href="javascript:;">
                                                                        <i class="far fa-heart"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="right_area">
                                                                <div class="content">
                                                                    <div class="left">
                                                                        <p>Golden Retriever<i class="far fa-venus ps-3"></i></p>
                                                                        <h4>£800.00</h4>
                                                                        <h5>Dublin, Ireland</h5>
                                                                        <h6>Lorem ipsum dolor et, Adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. </h6>
                                                                        <div class="button_area align_left">
                                                                            <ul>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Renew ad</a></li>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Bump ad</a></li>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Highlight ad</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right">
                                                                        <div class="icon_area">
                                                                            <ul>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-pen"></i></a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-trash-alt"></i></a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_area">
                                            <div class="row">
                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="ad_area">
                                                        <div class="inner_area">
                                                            <div class="left_area">
                                                                <img src="images/listdog4.png" alt="..." />
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="heart like">
                                                                    <a href="javascript:;">
                                                                        <i class="far fa-heart"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="right_area">
                                                                <div class="content">
                                                                    <div class="left">
                                                                        <p>Golden Retriever<i class="far fa-venus ps-3"></i></p>
                                                                        <h4>£800.00</h4>
                                                                        <h5>Dublin, Ireland</h5>
                                                                        <h6>Lorem ipsum dolor et, Adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. </h6>
                                                                        <div class="button_area align_left">
                                                                            <ul>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Renew ad</a></li>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Bump ad</a></li>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Highlight ad</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right">
                                                                        <div class="icon_area">
                                                                            <ul>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-pen"></i></a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-trash-alt"></i></a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_area">
                                            <div class="row">
                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="ad_area">
                                                        <div class="inner_area">
                                                            <div class="left_area">
                                                                <img src="images/dog3.png" alt="..." />
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="heart">
                                                                    <a href="javascript:;">
                                                                        <i class="far fa-heart"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="right_area">
                                                                <div class="content">
                                                                    <div class="left">
                                                                        <p>Golden Retriever<i class="far fa-venus ps-3"></i></p>
                                                                        <h4>£800.00</h4>
                                                                        <h5>Dublin, Ireland</h5>
                                                                        <h6>Lorem ipsum dolor et, Adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. </h6>
                                                                        <div class="button_area align_left">
                                                                            <ul>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Renew ad</a></li>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Bump ad</a></li>
                                                                                <li><a href="javascript:;" class="btn btn-primary-7">Highlight ad</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="right">
                                                                        <div class="icon_area">
                                                                            <ul>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-pen"></i></a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="javascript:;"><i class="fal fa-trash-alt"></i></a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-sold" role="tabpanel" aria-labelledby="pills-sold-tab">
                                        <div class="content_area">
                                            <div class="row">
                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="ad_area">
                                                        <div class="inner_area">
                                                            <div class="left_area">
                                                                <img src="images/dog.png" alt="..." />
                                                                <div class="flag">
                                                                    <img src="images/flag2.png" alt="..." />
                                                                </div>
                                                                <div class="heart">
                                                                    <a href="javascript:;">
                                                                        <i class="far fa-heart"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="right_area">
                                                                <div class="content">
                                                                    <div class="left top-padding">
                                                                        <p>Golden Retriever<i class="far fa-venus ps-3"></i></p>
                                                                        <h4>£800.00</h4>
                                                                        <h5>Dublin, Ireland</h5>
                                                                        <h6>Lorem ipsum dolor et, Adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. </h6>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="corner_title">
                                                            <p>Sold</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_area">
                                            <div class="row">
                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="ad_area">
                                                        <div class="inner_area">
                                                            <div class="left_area">
                                                                <img src="images/listdog4.png" alt="..." />
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="heart">
                                                                    <a href="javascript:;">
                                                                        <i class="far fa-heart"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="right_area">
                                                                <div class="content">
                                                                    <div class="left top-padding">
                                                                        <p>Golden Retriever<i class="far fa-venus ps-3"></i></p>
                                                                        <h4>£800.00</h4>
                                                                        <h5>Dublin, Ireland</h5>
                                                                        <h6>Lorem ipsum dolor et, Adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. </h6>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="corner_title">
                                                            <p>Sold</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_area">
                                            <div class="row">
                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="ad_area">
                                                        <div class="inner_area">
                                                            <div class="left_area">
                                                                <img src="images/dog3.png" alt="..." />
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="heart">
                                                                    <a href="javascript:;">
                                                                        <i class="far fa-heart"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="right_area">
                                                                <div class="content">
                                                                    <div class="left top-padding">
                                                                        <p>Golden Retriever<i class="far fa-venus ps-3"></i></p>
                                                                        <h4>£800.00</h4>
                                                                        <h5>Dublin, Ireland</h5>
                                                                        <h6>Lorem ipsum dolor et, Adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. </h6>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="corner_title">
                                                            <p>Sold</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ==== Footer ==== -->
<?php include('common/footer_2.php') ?>
<!-- ==== footer === -->
<?php include('common/footer.php') ?>
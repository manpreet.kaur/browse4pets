<!-- ==== Header === -->
<?php include('common/header.php') ?>

<section class="login_section top-space el dl">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mx-auto">
                <div class="login_wrap">
                    <div class="login_section_area">
                        <div class="header_area">
                            <h1>Forgot password</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            </p>
                            <div class="header_image1">
                                <img src="images/vector1.png" alt="..." />
                            </div>
                        </div>
                        <div class="box_area">
                            <form>
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="password">Email </label>
                                            <input type="email" class="form-control" placeholder="Enter your email address" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="login_button">
                                            <a href="javascript:;" class="btn btn-primary">Send Link</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="image_4">
        <img src="images/vector2.png" alt="..." />
    </div>
    <div class="image_5">
        <img src="images/vector3.png" alt="..." />
    </div>
</section>

<section class="foot_forget_img">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="foot_img_forget">
                    <div class="img_area">
                        <img src="images/OE-pug-1.png" alt=".." />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ==== footer === -->
<?php include('common/footer.php') ?>
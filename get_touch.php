<!-- ==== Header_section start === -->
<?php include('common/header.php') ?>
<section class="breadcame_section top-space">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="banner_area">
                    <div class="inner_area">
                        <h2>Get in touch</h2>
                        <div class="header_img d-lg-block d-none">
                            <img src="images/Vector6.png" alt="..." />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Header_section end === -->

<!-- ==== Details_section_start ==== -->
<section class="form_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="row">
                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="image_area">
                            <img src="images/dog_group.png" alt="..." />
                        </div>
                    </div>
                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="form_area">
                            <form>
                                <div class="row">
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="first_name">First  name</label>
                                            <input type="text" class="form-control" placeholder="First name" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="Last_name">Last  name</label>
                                            <input type="text" class="form-control" placeholder="Last name" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="email">Email address</label>
                                            <input type="email" class="form-control" placeholder="Email address" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="subject">Subject</label>
                                            <input type="text" class="form-control" placeholder="Subject" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="message">Message</label>
                                            <textarea type="text" class="form-control" placeholder="Enter here..." autocomplete="off" ></textarea>
                                        </div>
                                    </div>

                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="l-1" name="district[]" value="1" checked="">
                                                <label class="custom-control-label" for="l-1">Sign me up to receive email updates on new ads, upcoming litters and availability of pets.</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="sending_button">
                                            <a href="javascript:;" class="btn btn-primary">Send Message</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
    <div class="circle_image d-lg-block d-none">
        <img src="images/Vector2.png" alt="..." />
    </div>
</section>

<!-- ==== Details_section_end ==== -->


<!-- ==== footer === -->
<?php include('common/footer.php') ?>
<!-- ==== Header === -->
<?php include('common/header.php') ?>

<section class="submit_listing_section top-space">
    <div class="container">
        <div class="row">
            <div class="col-xxl-10 col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12 mx-auto">
                <div class="submit_listing_area">
                    <div class="header_area">
                        <h1>Submit Your Listing</h1>
                        <p>Please fill in the fields below to post your classified ad.
                        </p>
                        <div class="header_image1">
                            <img src="images/vector1.png" alt="..." />
                        </div>
                    </div>
                    <form>
                        <div class="form_step3">
                            <div class="box_area">
                                <div class="top_bar">
                                    <div class="line_bar">
                                        <div class="bar_inn" style="width:50%;"></div>
                                        <div class="bar_wrap">
                                            <div class="bar_circle circle_1"></div>
                                            <div class="bar_circle circle_2"></div>
                                            <div class="bar_circle circle_3"></div>
                                            <div class="bar_circle circle_4"></div>
                                            <div class="bar_circle circle_5"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="top_bar_name">
                                    <div class="bar_circle circle_1">Details</div>
                                    <div class="bar_circle circle_2">Photos</div>
                                    <div class="bar_circle circle_3">Health Tests</div>
                                    <div class="bar_circle circle_4">Pedigree</div>
                                    <div class="bar_circle circle_5">Plans</div>
                                </div>
                                <div class="row">
                                    <!-- <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="back_icon">
                                            <a href="photo.php"><i class="fal fa-long-arrow-left"></i><p>Back</p></a>
                                        </div>
                                    </div> -->
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="header">
                                            <h6>Health Test</h6>
                                            <div class="content">
                                                <p>Enter health tests for individual dog or parents of litter.  Health
                                                    tests will be displayed on ad only if certificate is uploaded</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <form>
                                            <div class="form_area">
                                                <div class="row">
                                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                                        <div class="form-group">    
                                                            <label for="sire">Sire</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xxl-9 col-xl-9 col-lg-9 col-md-9 col-sm-9 col-9">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" placeholder="KC Registered Name" autocomplete="off" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th width="30%">
                                                            Test 
                                                        </th>
                                                        <th width="24%">
                                                            Result 
                                                        </th>
                                                        <th width="24%">
                                                            Date 
                                                        </th>
                                                        <th width="6%">
                                                        </th>
                                                        <th width="6%">
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            sit door
                                                        </td>
                                                        <td>
                                                            amet
                                                        </td>
                                                        <td>
                                                            22/03/2022
                                                        </td>
                                                        <td>
                                                            <a href="javascript:;"><i class="far fa-eye"></i></a>
                                                        </td>
                                                        <td class="delete">
                                                            <a href="javascript:;"><i class="fas fa-trash-alt"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            sit door
                                                        </td>
                                                        <td>
                                                            amet
                                                        </td>
                                                        <td>
                                                            22/03/2022
                                                        </td>
                                                        <td>
                                                            <a href="javascript:;"><i class="far fa-eye"></i></a>
                                                        </td>
                                                        <td class="delete">
                                                            <a href="javascript:;"><i class="fas fa-trash-alt"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            sit door
                                                        </td>
                                                        <td>
                                                            amet
                                                        </td>
                                                        <td>
                                                            22/03/2022
                                                        </td>
                                                        <td>
                                                            <a href="javascript:;"><i class="far fa-eye"></i></a>
                                                        </td>
                                                        <td class="delete">
                                                            <a href="javascript:;"><i class="fas fa-trash-alt"></i></a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="icon_ad">
                                            <label for="file-input">
                                                <h5><a href="#" data-bs-toggle="modal" data-bs-target="#health_test"><i class="far fa-plus"></i>Add test</a></h5>
                                            </label>
                                            <input id="file-input" class="d-none" type="file" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <form>
                                            <div class="form_area padding">
                                                <div class="row">
                                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                                        <div class="form-group">    
                                                            <label for="dam">Dam</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xxl-9 col-xl-9 col-lg-9 col-md-9 col-sm-9 col-9">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" placeholder="KC Registered Name" autocomplete="off" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th width="30%">
                                                            Test
                                                        </th>
                                                        <th width="24%">
                                                            Result 
                                                        </th>
                                                        <th width="24%">
                                                            Date 
                                                        </th>
                                                        <th width="6%">
                                                        </th>
                                                        <th width="6%">
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            sit door
                                                        </td>
                                                        <td>
                                                            amet
                                                        </td>
                                                        <td>
                                                            22/03/2022
                                                        </td>
                                                        <td>
                                                            <a href="javascript:;"><i class="far fa-eye"></i></a>
                                                        </td>
                                                        <td class="delete">
                                                            <a href="javascript:;"><i class="fas fa-trash-alt"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            sit door
                                                        </td>
                                                        <td>
                                                            amet
                                                        </td>
                                                        <td>
                                                            22/03/2022
                                                        </td>
                                                        <td>
                                                            <a href="javascript:;"><i class="far fa-eye"></i></a>
                                                        </td>
                                                        <td class="delete">
                                                            <a href="javascript:;"><i class="fas fa-trash-alt"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            sit door
                                                        </td>
                                                        <td>
                                                            amet
                                                        </td>
                                                        <td>
                                                            22/03/2022
                                                        </td>
                                                        <td>
                                                            <a href="javascript:;"><i class="far fa-eye"></i></a>
                                                        </td>
                                                        <td class="delete">
                                                            <a href="javascript:;"><i class="fas fa-trash-alt"></i></a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="icon_ad">
                                            <label for="file-input">
                                                <h5><a href="#" data-bs-toggle="modal" data-bs-target="#health_test"><i class="far fa-plus"></i>Add test</a></h5>
                                            </label>
                                            <input id="file-input" class="d-none" type="file" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="button_area">
                                            <div class="left">
                                                <ul>
                                                    <li>
                                                        <a href="photo.php" class="btn btn-primary back" ><i class="fal fa-long-arrow-left pe-2"></i>Back</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;" class="btn btn-primary back back_steps_2">Save Draft</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="right">
                                                <a href="pedigree.php" class="btn btn-primary next_steps_3">Continue</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="image_2 d-lg-block d-none">
        <img src="images/vector2.png" alt="..." />
    </div>
    <div class="image_3 d-lg-block d-none">
        <img src="images/vector3.png" alt="..." />
    </div>
</section>





<!-- ==== footer === -->
<?php include('common/footer.php') ?>
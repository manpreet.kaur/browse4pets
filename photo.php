<!-- ==== Header === -->
<?php include('common/header.php') ?>

<section class="submit_listing_section top-space">
    <div class="container">
        <div class="row">
            <div class="col-xxl-10 col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12 mx-auto">
                <div class="submit_listing_area">
                    <div class="header_area">
                        <h1>Submit Your Listing</h1>
                        <p>Please fill in the fields below to post your classified ad.
                        </p>
                        <div class="header_image1">
                            <img src="images/vector1.png" alt="..." />
                        </div>
                    </div>
                    <form>
                        <div class="form_step2">
                            <div class="box_area">
                                <div class="top_bar">
                                    <div class="line_bar">
                                        <div class="bar_inn" style="width:24%;"></div>
                                        <div class="bar_wrap">
                                            <div class="bar_circle circle_1"></div>
                                            <div class="bar_circle circle_2"></div>
                                            <div class="bar_circle circle_3"></div>
                                            <div class="bar_circle circle_4"></div>
                                            <div class="bar_circle circle_5"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="top_bar_name">
                                    <div class="bar_circle circle_1">Details</div>
                                    <div class="bar_circle circle_2">Photos</div>
                                    <div class="bar_circle circle_3">Health Tests</div>
                                    <div class="bar_circle circle_4">Pedigree</div>
                                    <div class="bar_circle circle_5">Plans</div>
                                </div>
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="heading">
                                            <p>Photos</p>
                                            <h6>
                                                Upload up to 8 photos (highlighted image displays on ad preview)
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-3 col-4">
                                        <div class="icon_add active">
                                            <label for="file-input">
                                                <div class="add ">
                                                    <i class="fal fa-camera"></i>
                                                    <h5>Add photo</h5>
                                                </div>
                                            </label>
                                            <input id="file-input" class="d-none" type="file" />
                                            
                                            <div class="img_area d-none">
                                                <div class="inner_img">
                                                    <img src="images/listdog.png" alt=".." />
                                                </div>
                                                <div class="arrow_icon">
                                                   <a href="javascript:;"><i class="far fa-arrows"></i></a>
                                                </div>
                                                <div class="cross_icon">
                                                    <a href="javascript:;"><i class="fal fa-times"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-3 col-4">
                                        <div class="icon_add">
                                            <label for="file-input">
                                                <div class="add ">
                                                    <i class="fal fa-camera"></i>
                                                    <h5>Add photo</h5>
                                                </div>
                                            </label>
                                            <input id="file-input" class="d-none" type="file" />
                                            
                                            <div class="img_area d-none">
                                                <div class="inner_img">
                                                    <img src="images/listdog.png" alt=".." />
                                                </div>
                                                <div class="arrow_icon">
                                                   <a href="javascript:;"><i class="far fa-arrows"></i></a>
                                                </div>
                                                <div class="cross_icon">
                                                    <a href="javascript:;"><i class="fal fa-times"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-3 col-4">
                                        <div class="icon_add">
                                            <label for="file-input">
                                                <div class="add ">
                                                    <i class="fal fa-camera"></i>
                                                    <h5>Add photo</h5>
                                                </div>
                                            </label>
                                            <input id="file-input" class="d-none" type="file" />
                                            
                                            <div class="img_area d-none">
                                                <div class="inner_img">
                                                    <img src="images/listdog.png" alt=".." />
                                                </div>
                                                <div class="arrow_icon">
                                                   <a href="javascript:;"><i class="far fa-arrows"></i></a>
                                                </div>
                                                <div class="cross_icon">
                                                    <a href="javascript:;"><i class="fal fa-times"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-3 col-4">
                                        <div class="icon_add">
                                            <label for="file-input">
                                                <div class="add ">
                                                    <i class="fal fa-camera"></i>
                                                    <h5>Add photo</h5>
                                                </div>
                                            </label>
                                            <input id="file-input" class="d-none" type="file" />
                                            
                                            <div class="img_area d-none">
                                                <div class="inner_img">
                                                    <img src="images/listdog.png" alt=".." />
                                                </div>
                                                <div class="arrow_icon">
                                                   <a href="javascript:;"><i class="far fa-arrows"></i></a>
                                                </div>
                                                <div class="cross_icon">
                                                    <a href="javascript:;"><i class="fal fa-times"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-3 col-4">
                                        <div class="icon_add">
                                            <label for="file-input">
                                                <div class="add ">
                                                    <i class="fal fa-camera"></i>
                                                    <h5>Add photo</h5>
                                                </div>
                                            </label>
                                            <input id="file-input" class="d-none" type="file" />
                                            
                                            <div class="img_area d-none">
                                                <div class="inner_img">
                                                    <img src="images/listdog.png" alt=".." />
                                                </div>
                                                <div class="arrow_icon">
                                                   <a href="javascript:;"><i class="far fa-arrows"></i></a>
                                                </div>
                                                <div class="cross_icon">
                                                    <a href="javascript:;"><i class="fal fa-times"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-3 col-4">
                                        <div class="icon_add">
                                            <label for="file-input">
                                                <div class="add ">
                                                    <i class="fal fa-camera"></i>
                                                    <h5>Add photo</h5>
                                                </div>
                                            </label>
                                            <input id="file-input" class="d-none" type="file" />
                                            
                                            <div class="img_area d-none">
                                                <div class="inner_img">
                                                    <img src="images/listdog.png" alt=".." />
                                                </div>
                                                <div class="arrow_icon">
                                                   <a href="javascript:;"><i class="far fa-arrows"></i></a>
                                                </div>
                                                <div class="cross_icon">
                                                    <a href="javascript:;"><i class="fal fa-times"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-3 col-4">
                                        <div class="icon_add">
                                            <label for="file-input">
                                                <div class="add ">
                                                    <i class="fal fa-camera"></i>
                                                    <h5>Add photo</h5>
                                                </div>
                                            </label>
                                            <input id="file-input" class="d-none" type="file" />
                                            
                                            <div class="img_area d-none">
                                                <div class="inner_img">
                                                    <img src="images/listdog.png" alt=".." />
                                                </div>
                                                <div class="arrow_icon">
                                                   <a href="javascript:;"><i class="far fa-arrows"></i></a>
                                                </div>
                                                <div class="cross_icon">
                                                    <a href="javascript:;"><i class="fal fa-times"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-3 col-4">
                                        <div class="icon_add">
                                            <label for="file-input">
                                                <div class="add ">
                                                    <i class="fal fa-camera"></i>
                                                    <h5>Add photo</h5>
                                                </div>
                                            </label>
                                            <input id="file-input" class="d-none" type="file" />
                                            
                                            <div class="img_area d-none">
                                                <div class="inner_img">
                                                    <img src="images/listdog.png" alt=".." />
                                                </div>
                                                <div class="arrow_icon">
                                                   <a href="javascript:;"><i class="far fa-arrows"></i></a>
                                                </div>
                                                <div class="cross_icon">
                                                    <a href="javascript:;"><i class="fal fa-times"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="heading">
                                            <p>Videos</p>
                                            <h6>
                                                Upload up to 2 videos
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                        <div class="icon_add">
                                            <label for="file-input">
                                                <div class="add ">
                                                    <i class="fal fa-video"></i>
                                                    <h5>Add videos</h5>
                                                </div>
                                            </label>
                                            <input id="file-input" class="d-none" type="file" />
                                            
                                            <div class="img_area d-none">
                                                <div class="inner_img">
                                                    <img src="images/listdog.png" alt=".." />
                                                </div>
                                                <div class="arrow_icon">
                                                   <a href="javascript:;"><i class="far fa-arrows"></i></a>
                                                </div>
                                                <div class="cross_icon">
                                                    <a href="javascript:;"><i class="fal fa-times"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                        <div class="icon_add">
                                            <label for="file-input">
                                                <div class="add ">
                                                    <i class="fal fa-video"></i>
                                                    <h5>Add videos</h5>
                                                </div>
                                            </label>
                                            <input id="file-input" class="d-none" type="file" />
                                            
                                            <div class="img_area d-none">
                                                <div class="inner_img">
                                                    <img src="images/listdog.png" alt=".." />
                                                </div>
                                                <div class="arrow_icon">
                                                   <a href="javascript:;"><i class="far fa-arrows"></i></a>
                                                </div>
                                                <div class="cross_icon">
                                                    <a href="javascript:;"><i class="fal fa-times"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="button_area">
                                            <div class="left">
                                                <ul>
                                                    <li>
                                                        <a href="submit_listing.php" class="btn btn-primary back back_steps_1"><i class="fal fa-long-arrow-left pe-2"></i>Back</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;" class="btn btn-primary back">Save Draft</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="right">
                                                <a href="health_tests.php" class="btn btn-primary next_steps_2">Continue</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="image_2 d-lg-block d-none">
        <img src="images/vector2.png" alt="..." />
    </div>
    <div class="image_3 d-lg-block d-none">
        <img src="images/vector3.png" alt="..." />
    </div>
</section>





<!-- ==== footer === -->
<?php include('common/footer.php') ?>
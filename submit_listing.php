<!-- ==== Header === -->
<?php include('common/header.php') ?>

<section class="submit_listing_section top-space">
    <div class="container">
        <div class="row">
            <div class="col-xxl-10 col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12 mx-auto">
                <div class="submit_listing_area">
                    <div class="header_area">
                        <h1>Submit Your Listing</h1>
                        <p>Please fill in the fields below to post your classified ad.
                        </p>
                        <div class="header_image1">
                            <img src="images/vector1.png" alt="..." />
                        </div>
                    </div>
                    <form>
                        <div class="form_step1">
                            <div class="box_area">
                                <div class="top_bar">
                                <div class="line_bar">
                                        <div class="bar_inn" style="width:0%;"></div>
                                        <div class="bar_wrap">
                                            <div class="bar_circle circle_1"></div>
                                            <div class="bar_circle circle_2"></div>
                                            <div class="bar_circle circle_3"></div>
                                            <div class="bar_circle circle_4"></div>
                                            <div class="bar_circle circle_5"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="top_bar_name">
                                    <div class="bar_circle circle_1">Details</div>
                                    <div class="bar_circle circle_2">Photos</div>
                                    <div class="bar_circle circle_3">Health Tests</div>
                                    <div class="bar_circle circle_4">Pedigree</div>
                                    <div class="bar_circle circle_5">Plans</div>
                                </div>
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="advert_heading">Advert heading</label>
                                            <input type="text" class="form-control" placeholder="Enter Ad Title (include Breed)" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="breed">Breed</label>
                                            <select class="form-select">
                                                <option>Select Breed</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="category">Category</label>
                                            <select class="form-select">
                                                <option>Select Category</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="origin_country">Country of Origin</label>
                                            <input type="text" class="form-control" placeholder="Country of Origin" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h4>Date of Birth</h4>
                                    </div>
                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6">
                                        <div class="form-group">
                                            <label for="day">Day</label>
                                            <select class="form-select">
                                                <option>Day</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6">
                                        <div class="form-group">
                                            <label for="month">Month</label>
                                            <select class="form-select">
                                                <option>Month</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6">
                                        <div class="form-group">
                                            <label for="year">Year</label>
                                            <select class="form-select">
                                                <option>Year</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="locality">Locality</label>
                                            <input type="text" class="form-control" placeholder="Locality" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="county">County</label>
                                            <select class="form-select">
                                                <option>County</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-11 col-sm-11 col-11">
                                        <div class="form-group">
                                            <label for="price">Price</label>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="4-1" name="accessibility[]" value="1" checked />
                                                <label class="custom-control-label" for="4-1">Display Selling Price</label>
                                            </div>
                                            <input type="text" class="form-control padding" placeholder="Price" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="heading_area">
                                            <div class="left">
                                                <h5>Dog Microchip Details</h5>
                                            </div>
                                            <div class="right">
                                                <p>Steps of microchip verification</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="Microchip_registration">Microchip registration database</label>
                                            <select class="form-select">
                                                <option>Microchip registration database</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form_step3">
                                        <div class="box_area">
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="table-responsive">
                                                    <table class="table top_space">
                                                        <thead>
                                                            <tr>
                                                                <th width="30%">
                                                                    Microchip number 
                                                                </th>
                                                                <th width="24%">
                                                                    Gender 
                                                                </th>
                                                                <th width="24%">
                                                                    Color 
                                                                </th>
                                                                <th width="6%">
                                                                </th>
                                                                <th width="6%">
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    123456789125
                                                                </td>
                                                                <td>
                                                                    Male
                                                                </td>
                                                                <td>
                                                                    Black
                                                                </td>
                                                                <td>
                                                                    <a href="javascript:;" class="add"><i class="fal fa-pencil"></i></a>
                                                                </td>
                                                                <td class="color">
                                                                    <a href="javascript:;" class="add"><i class="fas fa-trash-alt"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    123456789125
                                                                </td>
                                                                <td>
                                                                    Male
                                                                </td>
                                                                <td>
                                                                    Golden
                                                                </td>
                                                                <td>
                                                                    <a href="javascript:;" class="add"><i class="fal fa-pencil"></i></a>
                                                                </td>
                                                                <td class="color">
                                                                    <a href="javascript:;" class="add"><i class="fas fa-trash-alt"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    123456789125
                                                                </td>
                                                                <td>
                                                                    Female
                                                                </td>
                                                                <td>
                                                                    White
                                                                </td>
                                                                <td>
                                                                    <a href="javascript:;" class="add"><i class="fal fa-pencil"></i></a>
                                                                </td>
                                                                <td class="color">
                                                                    <a href="javascript:;" class="add"><i class="fas fa-trash-alt"></i></a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="icon_ad bottom_space">
                                                    <label for="file-input">
                                                        <h5><a href="#" data-bs-toggle="modal" data-bs-target="#add_dog"><i class="far fa-plus"></i>Add dog</a></h5>
                                                    </label>
                                                    <input id="file-input" class="d-none" type="file" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h6>Instructions and Tips</h6>
                                        <div class="form-group">
                                            <textarea class="form-control plcholder" placeholder="Instructions and Tips"></textarea>
                                        </div>
                                    </div>
                                   
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="ragistration_heading">
                                            <h5>Registration & Health</h5>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="4-2" name="accessibility[]" value="1">
                                                <label class="custom-control-label" for="4-2">IKC Registered</label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="4-3" name="accessibility[]" value="1">
                                                <label class="custom-control-label" for="4-3">Vaccinated</label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="4-4" name="accessibility[]" value="1">
                                                <label class="custom-control-label" for="4-4">Wormed </label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="4-5" name="accessibility[]" value="1">
                                                <label class="custom-control-label" for="4-5">Neutered</label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="4-6" name="accessibility[]" value="1">
                                                <label class="custom-control-label" for="4-6">Vet Checked</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="description">Description</label>
                                            <textarea type="text" class="form-control" placeholder="Description" autocomplete="off" ></textarea>
                                        </div>
                                    </div> 
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="content">
                                            <p>The description is displayed in search results when people are searching for ads. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="button">
                                        <div class="left">
                                            <ul>
                                                <li>
                                                    <a href="ad_type.php" class="btn btn-primary back"><i class="fal fa-long-arrow-left pe-2"></i>Back</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;" class="btn btn-primary back">Save Draft</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="right">
                                            <a href="photo.php" class="btn btn-primary next_steps_1">Continue</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="image_2 d-xl-block d-none">
        <img src="images/vector2.png" alt="..." />
    </div>
    <div class="image_3 d-xl-block d-none">
        <img src="images/vector3.png" alt="..." />
    </div>
</section>





<!-- ==== footer === -->
<?php include('common/footer.php') ?>
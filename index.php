<!-- ==== Header === -->
<?php include('common/header.php') ?>

<section class="banner_section top-space" style="background-image: url('images/banner.png');" id="page1">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-md-12 col-sm-12 col-12">
                <div class="banner_content_side">
                    <div class="left">
                        <h1>
                           Find your new best friend
                        </h1>
                        <p>
                            Connect safely with a good breeder to find a happy healthy puppy 
                        </p>
                    </div> 
                </div>
                <div class="main_banner_dots_container">
                    <ul id="scoll_pagination_home">
                        <li class="first_element active">
                            <a href="#page1"></a>
                        </li>
                        <li class="second_element">
                            <a href="#page2"></a>
                        </li>
                        <li class="third_element">
                            <a href="#page3"></a>
                        </li>
                        <li class="fourth_element">
                            <a href="#page4"></a>
                        </li>
                        <li class="fifth_element">
                            <a href="#page5"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
</section>

<section class="search_bar">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-md-12 col-sm-12 col-12">
                <div class="search_bar_wrap">
                    <form action="">
                        <div class="type">
                            <div class="location">
                                <div class="form-group">
                                    <select class="form-select" id="validationCustom04" required>
                                        <option selected value="Location">Location</option>
                                        <option>...</option>
                                        <option>...</option>
                                        <option>...</option>
                                    </select>
                                    <div class="icon_wrap">
                                        <i class="fal fa-map-marker-alt"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="breed">
                                <div class="form-group">
                                    <select class="form-select" id="validationCustom04" required>
                                        <option selected value="Breed">Breed</option>
                                        <option>...</option>
                                    </select>
                                    <div class="icon_wrap d-sm-none d-block">
                                        <i class="fal fa-dog"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="search">
                            <div class="search_wrap">
                                <a href="#" class="btn btn-primary w-100">Search</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="highlights_section" id="page2">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="heading">
                    <h2>
                        Highlights
                    </h2>
                </div>
                <div class="highlights_wrap">
                    <div class="row">
                        <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="one_third">
                                <div class="img_area">
                                    <img src="images/dog.png" alt="..." />
                                </div>
                                <div class="detail">
                                    <div class="name">
                                        <div class="left">
                                            <p>
                                                Bichon Frise Golden 
                                            </p>
                                        </div>
                                        <div class="right">
                                            <i class="fal fa-mars"></i>
                                        </div>
                                    </div>
                                    <div class="loaction">
                                        <span>
                                            Dublin, Ireland
                                        </span>
                                        <h6>
                                            £800.00
                                        </h6>
                                    </div>
                                    <div class="price">
                                        <div class="visite">
                                            <a href="#">
                                                <i class="fal fa-long-arrow-up"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="favt">
                                    <a href="#">
                                        <i class="fal fa-heart"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="one_third">
                                <div class="img_area">
                                    <img src="images/dog2.png" alt="..." />
                                </div>
                                <div class="detail">
                                    <div class="name">
                                        <div class="left">
                                            <p>
                                                Bichon Frise Golden 
                                            </p>
                                        </div>
                                        <div class="right">
                                            <i class="fal fa-venus"></i>
                                        </div>
                                    </div>
                                    <div class="loaction">
                                        <span>
                                            Dublin, Ireland
                                        </span>
                                        <h6>
                                            £800.00
                                        </h6>
                                    </div>
                                    <div class="price">
                                        <div class="visite">
                                            <a href="#">
                                                <i class="fal fa-long-arrow-up"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="favt">
                                    <a href="#">
                                        <i class="fal fa-heart"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="one_third">
                                <div class="img_area">
                                    <img src="images/dog3.png" alt="..." />
                                </div>
                                <div class="detail">
                                    <div class="name">
                                        <div class="left">
                                            <p>
                                                Bichon Frise Golden 
                                            </p>
                                        </div>
                                        <div class="right">
                                            <i class="fal fa-venus"></i>
                                        </div>
                                    </div>
                                    <div class="loaction">
                                        <span>
                                            Dublin, Ireland
                                        </span>
                                        <h6>
                                            £800.00
                                        </h6>
                                    </div>
                                    <div class="price">
                                        <div class="visite">
                                            <a href="#">
                                                <i class="fal fa-long-arrow-up"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="favt">
                                    <a href="#">
                                        <i class="fal fa-heart"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="one_third">
                                <div class="img_area">
                                    <img src="images/dog4.png" alt="..." />
                                </div>
                                <div class="detail">
                                    <div class="name">
                                        <div class="left">
                                            <p>
                                                Bichon Frise Golden 
                                            </p>
                                        </div>
                                        <div class="right">
                                            <i class="fal fa-mars"></i>
                                        </div>
                                    </div>
                                    <div class="loaction">
                                        <span>
                                            Dublin, Ireland
                                        </span>
                                        <h6>
                                            £800.00
                                        </h6>
                                    </div>
                                    <div class="price">
                                        <div class="visite">
                                            <a href="#">
                                                <i class="fal fa-long-arrow-up"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="favt">
                                    <a href="#">
                                        <i class="fal fa-heart"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="listing_section" id="page3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="heading">
                    <h2>
                        Latest Listings
                    </h2>
                </div>
                <div class="highlights_wrap">
                    <div class="list_divide_main">
                        <div class="list_divide arrow_animate_items">
                            <div class="slick" id="listing">
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                            
                                            <img src="images/listdog.png" alt="..." />
                                            
                                            <div class="favt">
                                                <a href="dgdgdfg">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                            <div class="flag">
                                                <img src="images/flag1.png" alt="..."/>
                                            </div>
                                        </div>
                                        <div class="detail">
                                                <div class="name">
                                                <div class="left">
                                                    <p>
                                                        Bichon Frise Golden 
                                                    </p>
                                                </div>
                                                <div class="right">
                                                    <i class="fal fa-venus"></i>
                                                </div>
                                            </div>
                                            <div class="loaction">
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                                <h6>
                                                    £800.00
                                                </h6>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                           
                                                <img src="images/listdog.png" alt="..." />
                                          
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                            <div class="flag">
                                                <img src="images/flag2.png" alt="..."/>
                                            </div>
                                        </div>
                                        <div class="detail">
                                                <div class="name">
                                                <div class="left">
                                                    <p>
                                                        Bichon Frise Golden 
                                                    </p>
                                                </div>
                                                <div class="right">
                                                    <i class="fal fa-mars"></i>
                                                </div>
                                            </div>
                                            <div class="loaction">
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                                <h6>
                                                    £800.00
                                                </h6>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                           
                                                <img src="images/listdog.png" alt="..." />
                                           
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                            <div class="flag">
                                                <img src="images/flag2.png" alt="..."/>
                                            </div>
                                        </div>
                                        <div class="detail">
                                            <div class="name">
                                                <div class="left">
                                                    <p>
                                                        Bichon Frise Golden 
                                                    </p>
                                                </div>
                                                <div class="right">
                                                    <i class="fal fa-mars"></i>
                                                </div>
                                            </div>
                                            <div class="loaction">
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                                <h6>
                                                    £800.00
                                                </h6>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                           
                                                <img src="images/listdog4.png" alt="..." />
                                         
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                            <div class="flag">
                                                <img src="images/flag2.png" alt="..."/>
                                            </div>
                                        </div>
                                        <div class="detail">
                                                <div class="name">
                                                    <div class="left">
                                                        <p>
                                                            Bichon Frise Golden 
                                                        </p>
                                                    </div>
                                                    <div class="right">
                                                        <i class="fal fa-venus"></i>
                                                    </div>
                                                </div>
                                            <div class="loaction">
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                                <h6>
                                                    £800.00
                                                </h6>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                          
                                                <img src="images/listdog5.png" alt="..." />
                                          
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                            <div class="flag">
                                                <img src="images/flag2.png" alt="..."/>
                                            </div>
                                        </div>
                                        <div class="detail">
                                                <div class="name">
                                                <div class="left">
                                                    <p>
                                                        Bichon Frise Golden 
                                                    </p>
                                                </div>
                                                <div class="right">
                                                    <i class="fal fa-mars"></i>
                                                </div>
                                            </div>
                                            <div class="loaction">
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                                <h6>
                                                    £800.00
                                                </h6>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                           
                                                <img src="images/listdog6.png" alt="..." />
                                           
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                            <div class="flag">
                                                <img src="images/flag1.png" alt="..."/>
                                            </div>
                                        </div>
                                        <div class="detail">
                                                <div class="name">
                                                <div class="left">
                                                    <p>
                                                        Bichon Frise Golden 
                                                    </p>
                                                </div>
                                                <div class="right">
                                                    <i class="fal fa-mars"></i>
                                                </div>
                                            </div>
                                            <div class="loaction">
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                                <h6>
                                                    £800.00
                                                </h6>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                           
                                                <img src="images/listdog7.png" alt="..." />
                                         
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                            <div class="flag">
                                                <img src="images/flag1.png" alt="..."/>
                                            </div>
                                        </div>
                                        <div class="detail">
                                                <div class="name">
                                                <div class="left">
                                                    <p>
                                                        Bichon Frise Golden 
                                                    </p>
                                                </div>
                                                <div class="right">
                                                    <i class="fal fa-venus"></i>
                                                </div>
                                            </div>
                                            <div class="loaction">
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                                <h6>
                                                    £800.00
                                                </h6>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                           
                                                <img src="images/listdog8.png" alt="..." />
                                          
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                            <div class="flag">
                                                <img src="images/flag2.png" alt="..."/>
                                            </div>
                                        </div>
                                        <div class="detail">
                                                <div class="name">
                                                <div class="left">
                                                    <p>
                                                        Bichon Frise Golden 
                                                    </p>
                                                </div>
                                                <div class="right">
                                                    <i class="fal fa-venus"></i>
                                                </div>
                                            </div>
                                            <div class="loaction">
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                                <h6>
                                                    £800.00
                                                </h6>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                            
                                                <img src="images/listdog9.png" alt="..." />
                                         
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                            <div class="flag">
                                                <img src="images/flag2.png" alt="..."/>
                                            </div>
                                        </div>
                                        <div class="detail">
                                                <div class="name">
                                                <div class="left">
                                                    <p>
                                                        Bichon Frise Golden 
                                                    </p>
                                                </div>
                                                <div class="right">
                                                    <i class="fal fa-mars"></i>
                                                </div>
                                            </div>
                                            <div class="loaction">
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                                <h6>
                                                    £800.00
                                                </h6>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                           
                                                <img src="images/listdog6.png" alt="..." />
                                        
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                            <div class="flag">
                                                <img src="images/flag2.png" alt="..."/>
                                            </div>
                                        </div>
                                        <div class="detail">
                                                <div class="name">
                                                <div class="left">
                                                    <p>
                                                        Bichon Frise Golden 
                                                    </p>
                                                </div>
                                                <div class="right">
                                                    <i class="fal fa-mars"></i>
                                                </div>
                                            </div>
                                            <div class="loaction">
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                                <h6>
                                                    £800.00
                                                </h6>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                          
                                                <img src="images/listdog.png" alt="..." />
                                          
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="detail">
                                                <div class="name">
                                                <div class="left">
                                                    <p>
                                                        Bichon Frise Golden 
                                                    </p>
                                                </div>
                                                <div class="right">
                                                    <i class="fal fa-mars"></i>
                                                </div>
                                            </div>
                                            <div class="loaction">
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                                <h6>
                                                    £800.00
                                                </h6>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                           
                                                <img src="images/listdog.png" alt="..." />
                                          
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="detail">
                                                <div class="name">
                                                <div class="left">
                                                    <p>
                                                        Bichon Frise Golden 
                                                    </p>
                                                </div>
                                                <div class="right">
                                                    <i class="fal fa-mars"></i>
                                                </div>
                                            </div>
                                            <div class="loaction">
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                                <h6>
                                                    £800.00
                                                </h6>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                         
                                                <img src="images/listdog4.png" alt="..." />
                                           
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="detail">
                                                <div class="name">
                                                <div class="left">
                                                    <p>
                                                        Bichon Frise Golden 
                                                    </p>
                                                </div>
                                                <div class="right">
                                                    <i class="fal fa-mars"></i>
                                                </div>
                                            </div>
                                            <div class="loaction">
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                                <h6>
                                                    £800.00
                                                </h6>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                           
                                                <img src="images/listdog.png" alt="..." />
                                          
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="detail">
                                                <div class="name">
                                                <div class="left">
                                                    <p>
                                                        Bichon Frise Golden 
                                                    </p>
                                                </div>
                                                <div class="right">
                                                    <i class="fal fa-mars"></i>
                                                </div>
                                            </div>
                                            <div class="loaction">
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                                <h6>
                                                    £800.00
                                                </h6>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                           
                                                <img src="images/listdog.png" alt="..." />
                                          
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="detail">
                                                <div class="name">
                                                <div class="left">
                                                    <p>
                                                        Bichon Frise Golden 
                                                    </p>
                                                </div>
                                                <div class="right">
                                                    <i class="fal fa-mars"></i>
                                                </div>
                                            </div>
                                            <div class="loaction">
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                                <h6>
                                                    £800.00
                                                </h6>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                           
                                                <img src="images/listdog4.png" alt="..." />
                                           
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="detail">
                                                <div class="name">
                                                <div class="left">
                                                    <p>
                                                        Bichon Frise Golden 
                                                    </p>
                                                </div>
                                                <div class="right">
                                                    <i class="fal fa-mars"></i>
                                                </div>
                                            </div>
                                            <div class="loaction">
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                                <h6>
                                                    £800.00
                                                </h6>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="have_found_section" style="background-image: url('images/Rectangle-70.png');" id="page4">
    <div class="container">
        <div class="row ">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="found_set">
                    <h2>
                    Haven't found your Best friend yet?
                    </h2>
                    <div class="miss_out" data-bs-toggle="modal" data-bs-target="#get_notified">
                        <a href="#" class="btn btn-primary">
                            Don't miss out <i class="fal fa-long-arrow-up"></i>
                        </a>
                    </div>
                </div>
                <div class="img_warp">
                    <img src="images/foun.png" alt=".." />
                </div>
                <div class="main_banner_dots_container">
                    <ul id="scoll_pagination_home">
                        <li class="first_element active">
                            <a href="#page1"></a>
                        </li>
                        <li class="second_element">
                            <a href="#page2"></a>
                        </li>
                        <li class="third_element">
                            <a href="#page3"></a>
                        </li>
                        <li class="fourth_element">
                            <a href="#page4"></a>
                        </li>
                        <li class="fifth_element">
                            <a href="#page5"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="listing_section breeders_listing_section homes">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="heading">
                    <h2>
                        Breeders
                    </h2>
                </div>
                <div class="highlights_wrap">
                    <div class="list_divide_main">
                        <div class="list_divide">
                            <div class="breeders_slick">
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                            <img src="images/listdog.png" alt="..." />
                                            <div class="favt">
                                                <a href="dgdgdfg">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="detail">
                                            <div class="name">
                                                <p>
                                                    Bichon Frise Golden 
                                                </p>
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="wfwefwfwea">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                            <img src="images/listdog.png" alt="..." />
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="detail">
                                            <div class="name">
                                                <p>
                                                    Bichon Frise Golden 
                                                </p>
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="#">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                            <img src="images/listdog.png" alt="..." />
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="detail">
                                            <div class="name">
                                                <p>
                                                    Bichon Frise Golden 
                                                </p>
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="#">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                            <img src="images/listdog4.png" alt="..." />
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="detail">
                                            <div class="name">
                                                <p>
                                                    Bichon Frise Golden 
                                                </p>
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="#">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="one_third">
                                        <div class="img_area">
                                            <img src="images/listdog5.png" alt="..." />
                                            <div class="favt">
                                                <a href="#">
                                                    <i class="fal fa-heart"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="detail">
                                            <div class="name">
                                                <p>
                                                    Bichon Frise Golden 
                                                </p>
                                                <span>
                                                    Dublin, Ireland
                                                </span>
                                            </div>
                                            <div class="price">
                                                <div class="visite">
                                                    <a href="#">
                                                        <i class="fal fa-long-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="safte_section" id="page5">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="heading">
                    <h2>
                        SAFE<span>4</span>dogs
                    </h2>
                </div>
                <div class="safte_reviews">
                    <div class="safte_warp">
                        <div class="owl-carousel owl-theme" id="safte">
                            <div class="item">
                                <div class="onw_third">
                                    <div class="view">
                                        <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vitae neque malesuada, vulputate mi sit amet, sollicitudin dolor. Mauris vulputate mattis mauris, eget tincidunt nisi mollis vel.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        </p>
                                    </div>
                                    <div class="user">
                                        <div class="left">
                                            <div class="img_wrap">
                                                <img src="images/user.png" alt=".." />
                                            </div>
                                        </div>
                                        <div class="right">
                                            <div class="name">
                                                <p>
                                                Veet baljit
                                                </p>
                                                <span>
                                                Lorem ipsum dolor, amet 
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="onw_third">
                                    <div class="view">
                                        <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vitae neque malesuada, vulputate mi sit amet, sollicitudin dolor. Mauris vulputate mattis mauris, eget tincidunt nisi mollis vel.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        </p>
                                    </div>
                                    <div class="user">
                                        <div class="left">
                                            <div class="img_wrap">
                                                <img src="images/user.png" alt=".." />
                                            </div>
                                        </div>
                                        <div class="right">
                                            <div class="name">
                                                <p>
                                                Veet baljit
                                                </p>
                                                <span>
                                                Lorem ipsum dolor, amet 
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="onw_third">
                                    <div class="view">
                                        <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vitae neque malesuada, vulputate mi sit amet, sollicitudin dolor. Mauris vulputate mattis mauris, eget tincidunt nisi mollis vel.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        </p>
                                    </div>
                                    <div class="user">
                                        <div class="left">
                                            <div class="img_wrap">
                                                <img src="images/user.png" alt=".." />
                                            </div>
                                        </div>
                                        <div class="right">
                                            <div class="name">
                                                <p>
                                                Veet baljit
                                                </p>
                                                <span>
                                                Lorem ipsum dolor, amet 
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="onw_third">
                                    <div class="view">
                                        <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vitae neque malesuada, vulputate mi sit amet, sollicitudin dolor. Mauris vulputate mattis mauris, eget tincidunt nisi mollis vel.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        </p>
                                    </div>
                                    <div class="user">
                                        <div class="left">
                                            <div class="img_wrap">
                                                <img src="images/user.png" alt=".." />
                                            </div>
                                        </div>
                                        <div class="right">
                                            <div class="name">
                                                <p>
                                                Veet baljit
                                                </p>
                                                <span>
                                                Lorem ipsum dolor, amet 
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="foot_over">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="img_area">
                    <img src="images/OE-mixed4.png" alt=".." />
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ==== footer === -->
<?php include('common/footer.php') ?>
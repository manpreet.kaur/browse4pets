<!-- ==== Header === -->
<?php include('common/header.php') ?>
<!-- ==== Header === -->
<?php include('common/header3.php') ?>

<section class="dashboard_section">
    <div class="dashboard_inner top-space">
        <?php include('dashboard_sidebar.php') ?>
        <div class="right_side_wrap">
            <div class="setting">
                <div class="breadcame margin">
                    <div class="breadcame_area">
                        <h2>Messages</h2>
                    </div> 
                </div>
                <div class="message_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="content_box">
                                    <div class="row">
                                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                            <div class="left_content_box">
                                                <form>
                                                    <input type="text" class="form-control" placeholder="Search">
                                                </form>
                                                <div class="left_area">
                                                    <img src="images/dog2.png" alt="..." />
                                                </div>
                                                <div class="right_area">
                                                    <div class="left">
                                                        <p>Dianne Jane</p>
                                                        <p>Bichon Frise Golden</p>
                                                        <h5>Lorem ipsum dolor sit amet.</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ==== Footer ==== -->
<?php include('common/footer_2.php') ?>
<!-- ==== footer === -->
<?php include('common/footer.php') ?>
<!-- ==== Header === -->
<?php include('common/header.php') ?>

<section class="login_section top-space el dl">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mx-auto">
                <div class="login_wrap">
                    <div class="login_section_area">
                        <div class="header_area">
                            <h1>Login</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            </p>
                            <div class="header_image1">
                                <img src="images/vector1.png" alt="..." />
                            </div>
                        </div>
                        <div class="box_area">
                            <h2>Welcome back!</h2>
                            <form>
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="user_name">Email</label>
                                            <input type="text" class="form-control" placeholder="Enter username or email" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control" placeholder="Enter password" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="forgot_button">
                                            <a href="forget_password.php">Forgot password?</a>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="login_button">
                                            <a href="javascript:;" class="btn btn-primary">Login</a>
                                        </div>
                                        <div class="sign_up_button">
                                            <p>Don't have an account? <a href="signup.php">Sign Up</a></p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="foot_img d-lg-block d-none">
                        <div class="img_area">
                            <img src="images/JRT-upwall.png" alt=".." />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="image_2">
        <img src="images/vector2.png" alt="..." />
    </div>
    <div class="image_3">
        <img src="images/vector3.png" alt="..." />
    </div>
</section>





<!-- ==== footer === -->
<?php include('common/footer.php') ?>
<!-- ==== Header === -->
<?php include('common/header.php') ?>
<section class="detail_section top-space">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="detail_area">
                    <div class="row">
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="detail_slider sticky-md-top">
                                <div class="detail_page_start">
                                    <div class="product_images">
                                        <div id="sync1" class="owl-carousel-cc">
                                            <div class="item">
                                                <div class="single_image">
                                                    <img src="images/1.jpg" alt="..." />
                                                    <div class="incon">
                                                        <a href="javascript:;"><i class="far fa-heart"></i></a>
                                                    </div>
                                                    <div class="flag">
                                                        <img src="images/flag1.png" alt="..."/>
                                                    </div>
                                                    <div class="corner_info">
                                                        <p>Available</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="single_image">
                                                    <img src="images/2.jpg" alt="..." />
                                                    <div class="incon">
                                                        <a href="javascript:;"><i class="far fa-heart"></i></a>
                                                    </div>
                                                    <div class="flag">
                                                        <img src="images/flag1.png" alt="..."/>
                                                    </div>
                                                    <div class="corner_info">
                                                        <p>Available</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="single_image">
                                                    <img src="images/3.jpg" alt="..." />
                                                    <div class="incon">
                                                        <a href="javascript:;"><i class="far fa-heart"></i></a>
                                                    </div>
                                                    <div class="flag">
                                                        <img src="images/flag1.png" alt="..."/>
                                                    </div>
                                                    <div class="corner_info">
                                                        <p>Available</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="single_image">
                                                    <img src="images/4.jpg" alt="..." />
                                                    <div class="incon">
                                                        <a href="javascript:;"><i class="far fa-heart"></i></a>
                                                    </div>
                                                    <div class="flag">
                                                        <img src="images/flag1.png" alt="..."/>
                                                    </div>
                                                    <div class="corner_info">
                                                        <p>Available</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="single_image">
                                                    <img src="images/6.jpg" alt="..." />
                                                    <div class="incon">
                                                        <a href="javascript:;"><i class="far fa-heart"></i></a>
                                                    </div>
                                                    <div class="flag">
                                                        <img src="images/flag1.png" alt="..."/>
                                                    </div>
                                                    <div class="corner_info">
                                                        <p>Available</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="single_image">
                                                    <img src="images/7.jpg" alt="..." />
                                                    <div class="incon">
                                                        <a href="javascript:;"><i class="far fa-heart"></i></a>
                                                    </div>
                                                    <div class="flag">
                                                        <img src="images/flag1.png" alt="..."/>
                                                    </div>
                                                    <div class="corner_info">
                                                        <p>Available</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="single_image">
                                                    <img src="images/8.jpg" alt="..." />
                                                    <div class="incon">
                                                        <a href="javascript:;"><i class="far fa-heart"></i></a>
                                                    </div>
                                                    <div class="flag">
                                                        <img src="images/flag1.png" alt="..."/>
                                                    </div>
                                                    <div class="corner_info">
                                                        <p>Available</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="sync2" class="owl-carousel-cc">
                                            <div class="item">
                                                <div class="botttom_image">
                                                    <img src="images/1.jpg" alt="..." />
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="botttom_image">
                                                    <img src="images/2.jpg" alt="..." />
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="botttom_image">
                                                    <img src="images/3.jpg" alt="..." />
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="botttom_image">
                                                    <img src="images/4.jpg" alt="..." />
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="botttom_image">
                                                    <img src="images/6.jpg" alt="..." />
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="botttom_image">
                                                    <img src="images/7.jpg" alt="..." />
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="botttom_image">
                                                    <img src="images/8.jpg" alt="..." />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="content_area">
                                <div class="header_area">
                                    <div class="left">
                                        <p>10 hours ago</p>
                                        <a href="javascript:;">
                                            <h6><i class="far fa-eye pe-2"></i>
                                            25 viewed this add
                                            </h6>
                                        </a>
                                    </div>
                                    <div class="right">
                                        <div class="dropdown">
                                            <button type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                <i class="far fa-ellipsis-v"></i>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <li><a class="dropdown-item" href="#">Report Add</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="inner_area">
                                    <h2>
                                        Beautiful IKC Reg Golden Retriever Puppies for sale 
                                    </h2>
                                    <div class="price">
                                        <h2>£800.00</h2>
                                    </div>
                                    <p>Dublin, Ireland 
                                    </p>
                                    <div class="icon">
                                        <i class="fal fa-venus-mars"></i>
                                    </div>
                                </div>
                                <div class="map_area">
                                    <div class="left">
                                        <div class="image">
                                            <img src="images/user.png" alt="..." />
                                        </div>
                                        <h6>Ralph Edwards</h6>
                                        <p>155649562189</p>
                                        <p>Dublin, Ireland</p>
                                    </div>
                                    <div class="right">
                                        <div class="map">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2437885.707017297!2d-10.459920046846163!3d53.36566901862628!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4859bae45c4027fb%3A0xcf7c1234cedbf408!2sIreland!5e0!3m2!1sen!2sin!4v1656767917049!5m2!1sen!2sin" width="100%"  style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="button_area">
                                    <ul>
                                        <li><a href="javascript:;" class="btn btn-primary">Contact</a></li>
                                        <li><a href="javascript:;" class="btn btn-primary-7">Get directions</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="description_area">
                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Description
                            </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <p>Lorem ipsum dolor et, consectetur adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amrem ipsum dolor sit amet, elit.Lorem ipsum dolor e</p>
                                    <p>t, consectetur adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amrem ipsum dolor sit amet, elit.Lorem ipsum dolor et, consectetur adipiscing elit. 
                                        Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amrem ipsum dolor sit amet,</p>
                                    <p>elit.Lorem ipsum dolor et, consectetur adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit am</p>
                                    <p>rem ipsum dolor sit amet, elit.Lorem ipsum dolor et, consectetur adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amrem ibmnj,b,j.kn.k.kmlm;/lb’/</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="champion_btn">
                    <a href="javascript:;" class="btn btn-primary-8">Champion Bloodlines
                        <div class="image">
                            <img src="images/trophy_1.png" alt="..." />
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="information_area">
                    <div class="row">
                        <div class="col-xxl-6 col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="left_side">
                                <div class="description_area">
                                    <div class="accordion" id="accordionExample">
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingOne">
                                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                                                Additional Info
                                            </button>
                                            </h2>
                                            <div id="collapseTwo" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <div class="add_info">
                                                        <div class="left">
                                                            <p>Breed :</p>
                                                        </div>
                                                        <div class="right">
                                                            <p>Golden retriever</p>
                                                        </div>
                                                    </div>
                                                    <div class="add_info">
                                                        <div class="left">
                                                            <p>Color :</p>
                                                        </div>
                                                        <div class="right">
                                                            <p>Golden</p>
                                                        </div>
                                                    </div>
                                                    <div class="add_info">
                                                        <div class="left">
                                                            <p>Date of birth :</p>
                                                        </div>
                                                        <div class="right">
                                                            <p>25/01/2022 (3 months)</p>
                                                        </div>
                                                    </div>
                                                    <div class="add_info">
                                                        <div class="left">
                                                            <p>Country :</p>
                                                        </div>
                                                        <div class="right">
                                                            <p>Ireland</p>
                                                        </div>
                                                    </div>
                                                    <div class="add_info">
                                                        <div class="left">
                                                            <p>Kennel name :</p>
                                                        </div>
                                                        <div class="right">
                                                            <p>Lorem ipsum door</p>
                                                        </div>
                                                    </div>
                                                    <div class="add_info">
                                                        <div class="left">
                                                            <p>Seller ID :</p>
                                                        </div>
                                                        <div class="right">
                                                            <p>12423234</p>
                                                        </div>
                                                    </div>
                                                    <div class="add_info">
                                                        <div class="left">
                                                            <p>Max No of Breeding Bitches :</p>
                                                        </div>
                                                        <div class="right">
                                                            <p>123</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="right_side">
                                <div class="description_area">
                                    <div class="accordion" id="accordionExample">
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingOne">
                                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="true" aria-controls="collapseOne">
                                                Microchip Details
                                            </button>
                                            </h2>
                                            <div id="collapseThree" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <div class="microchip_detail">
                                                        <div class="table_area">
                                                            <div class="table_row_area">
                                                                <div class="table-responsive">
                                                                    <table class="table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>

                                                                                </th>
                                                                                <th>
                                                                                    Microchip Number
                                                                                </th>
                                                                                <th>
                                                                                    Gender
                                                                                </th>
                                                                                <th>
                                                                                    Color
                                                                                </th>
                                                                                <th>
                                                                                    Status
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr class="abc">
                                                                                <td valign="middle">
                                                                                    <i class="fal fa-check"></i>
                                                                                </td>
                                                                                <td valign="middle">
                                                                                    <a href="javascript:;">-</a> 
                                                                                </td>
                                                                                <td valign="middle">
                                                                                    Male
                                                                                </td>
                                                                                <td valign="middle">Black</td>
                                                                                <td>
                                                                                    <div class="sold">
                                                                                        <p>Sold</p>
                                                                                    <div>
                                                                                </td>   
                                                                            </tr>
                                                                            <tr class="abc">
                                                                                <td valign="middle">
                                                                                    <i class="fal fa-check"></i>
                                                                                </td>
                                                                                <td valign="middle">
                                                                                    <a href="javascript:;">-</a> 
                                                                                </td>
                                                                                <td valign="middle">
                                                                                    Male
                                                                                </td>
                                                                                <td valign="middle">Golden</td>
                                                                                <td>
                                                                                    <div class="sold hold">
                                                                                        <p>On Hold</p>
                                                                                    </div>
                                                                                </td>   
                                                                            </tr>
                                                                            <tr class="abc">
                                                                                <td valign="middle">
                                                                                    <i class="fal fa-check"></i>
                                                                                </td>
                                                                                <td valign="middle">
                                                                                    <a href="javascript:;">123456789125</a> 
                                                                                </td>
                                                                                <td valign="middle">
                                                                                    Female
                                                                                </td>
                                                                                <td valign="middle">White</td>
                                                                                <td>
                                                                                    <div class="sold available">
                                                                                        <p>Available</p>
                                                                                    </div>
                                                                                </td>   
                                                                            </tr>
                                                                            <tr class="abc">
                                                                                <td valign="middle">
                                                                                    <i class="fal fa-check"></i>
                                                                                </td>
                                                                                <td valign="middle">
                                                                                    <a href="javascript:;">-</a> 
                                                                                </td>
                                                                                <td valign="middle">
                                                                                    Male
                                                                                </td>
                                                                                <td valign="middle">Black</td>
                                                                                <td>
                                                                                    <div class="sold">
                                                                                        <p>Sold</p>
                                                                                    </div>
                                                                                </td>   
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="checked_list_area">
                    <div class="inner_area">
                        <ul>
                            <li><i class="fal fa-check me-2"></i>KC Registered</li>
                            <li><i class="fal fa-check me-2"></i>Wormed Regularly</li>
                            <li><i class="fal fa-check me-2"></i>Part/FullyVaccinated</li>
                            <li><i class="fal fa-check me-2"></i>Vet Checked</li>
                            <li><i class="fal fa-check me-2"></i>Neutered</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="health_test_section">
                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="true" aria-controls="collapseOne">
                                    Health Tests
                                </button>
                            </h2>
                            <div id="collapseFour" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <div class="information_area health_test">
                                        <div class="row">
                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="right_side">
                                                    <div class="dog_namedd">
                                                        <p>
                                                            Rambo
                                                        </p>
                                                    </div>
                                                    <div class="description_area">
                                                        <div class="microchip_detail">
                                                            <div class="table_area">
                                                                <div class="table_row_area">
                                                                    <div class="table-responsive">
                                                                        <table class="table">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>
                                                                                        Test
                                                                                    </th>
                                                                                    <th>
                                                                                        Result
                                                                                    </th>
                                                                                    <th>
                                                                                        Date
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr class="abc">
                                                                                    <td>
                                                                                        sit door
                                                                                    </td>
                                                                                    <td>
                                                                                        amit
                                                                                    </td>
                                                                                    <td>
                                                                                        22/03/2022
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="abc">
                                                                                    <td>
                                                                                        sit door
                                                                                    </td>
                                                                                    <td>
                                                                                        amit
                                                                                    </td>
                                                                                    <td>
                                                                                        22/03/2022
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="abc">
                                                                                    <td>
                                                                                        sit door
                                                                                    </td>
                                                                                    <td>
                                                                                        amit
                                                                                    </td>
                                                                                    <td>
                                                                                        22/03/2022
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="right_side">
                                                <div class="dog_namedd">
                                                        <p>
                                                           Boozo
                                                        </p>
                                                    </div>
                                                    <div class="description_area">
                                                        <div class="microchip_detail">
                                                            <div class="table_area">
                                                                <div class="table_row_area">
                                                                    <div class="table-responsive">
                                                                        <table class="table">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>
                                                                                        Test
                                                                                    </th>
                                                                                    <th>
                                                                                        Result
                                                                                    </th>
                                                                                    <th>
                                                                                        Date
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr class="abc">
                                                                                    <td>
                                                                                        sit door
                                                                                    </td>
                                                                                    <td>
                                                                                        amit
                                                                                    </td>
                                                                                    <td>
                                                                                        22/03/2022
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="abc">
                                                                                    <td>
                                                                                        sit door
                                                                                    </td>
                                                                                    <td>
                                                                                        amit
                                                                                    </td>
                                                                                    <td>
                                                                                        22/03/2022
                                                                                    </td>
                                                                                </tr>
                                                                                <tr class="abc">
                                                                                    <td>
                                                                                        sit door
                                                                                    </td>
                                                                                    <td>
                                                                                        amit
                                                                                    </td>
                                                                                    <td>
                                                                                        22/03/2022
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="pedigree_area">
                    <div class="header">
                        <h6>Pedigree</h6>
                    </div>
                    <div class="content_area">
                        <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>
                                        Parents
                                    </td>
                                    <td>
                                        Grand Parents
                                    </td>
                                    <td>
                                        Great Grant Parents
                                    </td>
                                    <td>
                                        Great Great Grant Parents
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="8" valign="middle">
                                        Zejak Bubezi Bankole at Hooz
                                    </td>
                                    <td rowspan="4" valign="middle">
                                        Zejak Guy Rooi Van Rooyen
                                    </td>
                                    <td rowspan="2" valign="middle">
                                        Callum's Special Envoy
                                    </td>
                                    <td>
                                        Veldtkammer Tip Em Up
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="color">
                                            Ch Druimderg Dancin Wit Fire
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="2" valign="middle">
                                        Kweli Busara Great African Brown Sugar
                                    </td>
                                    <td>
                                        <div class="color">
                                            Ch Shangani Dundabala Weltevreden
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Ember Malaika From Mistery Castle
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="4" valign="middle">
                                        Zejak Bomvu Bubezi
                                    </td>
                                    <td rowspan="2" valign="middle">
                                        Veldtkammer Yumazin Zejak
                                    </td>
                                    <td>
                                        Veldtkammer Mega Hassle
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Hespa High As A Kite at Veldtkammer
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="2" valign="middle">
                                        Zejak Majuzi
                                    </td>
                                    <td>
                                        Veldtkammer Gorgeos Darlin'Zejak
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Zejak Cilombo
                                    </td>
                                </tr>


                                <tr>
                                    <td rowspan="8" valign="middle">
                                        Hooz Na La Z
                                    </td>
                                    <td rowspan="4" valign="middle">
                                        Hooz Ibuddy
                                    </td>
                                    <td rowspan="2" valign="middle">
                                        Hooz Veldt Viber Kammer
                                    </td>
                                    <td>
                                        Veldtkammer Get of A My Cloud
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Zhos Choki Hooz
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="2" valign="middle">
                                        Hooz Vao
                                    </td>
                                    <td>
                                        Veldtkammer Get of A My Cloud
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Show Red Jala Hooz
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="4" valign="middle">
                                        Hooz Looking Now
                                    </td>
                                    <td rowspan="2" valign="middle">
                                        Zejak Ini Jazaa Hooz
                                    </td>
                                    <td>
                                        <div class="color">
                                            Ch & Ir Ch Zejak Zuri Miko
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Zejak Ini Tedi
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="2" valign="middle">
                                        Hooz Kamaria
                                    </td>
                                    <td>
                                        Hooz Veldt Vibex Kammer
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Hooz Vao
                                    </td>
                                </tr>
                            <tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="social_icon_area">
                    <div class="inner_area">
                        <ul>
                            <li>
                                Share Ad
                            </li>
                            <li>
                                <a href="#"><i class="fab fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fal fa-envelope"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="listing_section breeders_listing_section search_result related_ads">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="releted_ads_area">
                    <div class="top_header">
                        <a href="#" data-bs-toggle="modal" data-bs-target="#instruction_tips"><h6>Instruction and tips for buying a dog</h6></a>
                    </div>
                    <div class="heading">
                        <h2>
                            Related Ads
                        </h2>
                    </div>
                    <div class="highlights_wrap">
                        <div class="list_divide_main">
                            <div class="list_divide">
                                <div class="breeders_slick"> 
                                    <div class="item">
                                        <div class="one_third match_height_col">
                                            <div class="img_area">
                                                <img src="images/listdog.png" alt="..." />
                                                <div class="favt">
                                                    <a href="#">
                                                        <i class="fal fa-heart"></i>
                                                    </a>
                                                </div>
                                                <div class="flag">
                                                    <img src="images/flag1.png" alt="..."/>
                                                </div>
                                            </div>
                                            <div class="detail">
                                                    <div class="name">
                                                    <div class="left">
                                                        <p>
                                                            Bichon Frise Golden 
                                                        </p>
                                                    </div>
                                                    <div class="right">
                                                        <i class="fal fa-venus"></i>
                                                    </div>
                                                </div>
                                                <div class="loaction">
                                                    <span>
                                                        Dublin, Ireland
                                                    </span>
                                                    <h6>
                                                        £800.00
                                                    </h6>
                                                </div>
                                                <div class="price">
                                                    <div class="visite">
                                                        <a href="#">
                                                            <i class="fal fa-long-arrow-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="one_third match_height_col">
                                            <div class="img_area">
                                                <img src="images/listdog3.png" alt="..." />
                                                <div class="favt">
                                                    <a href="#">
                                                        <i class="fal fa-heart"></i>
                                                    </a>
                                                </div>
                                                <div class="flag">
                                                    <img src="images/flag2.png" alt="..."/>
                                                </div>
                                            </div>
                                            <div class="detail">
                                                    <div class="name">
                                                    <div class="left">
                                                        <p>
                                                            Bichon Frise Golden 
                                                        </p>
                                                    </div>
                                                    <div class="right">
                                                        <i class="fal fa-venus"></i>
                                                    </div>
                                                </div>
                                                <div class="loaction">
                                                    <span>
                                                        Dublin, Ireland
                                                    </span>
                                                    <h6>
                                                        £800.00
                                                    </h6>
                                                </div>
                                                <div class="price">
                                                    <div class="visite">
                                                        <a href="#">
                                                            <i class="fal fa-long-arrow-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="one_third match_height_col">
                                            <div class="img_area">
                                                <img src="images/listdog2.png" alt="..." />
                                                <div class="favt">
                                                    <a href="#">
                                                        <i class="fal fa-heart"></i>
                                                    </a>
                                                </div>
                                                <div class="flag">
                                                    <img src="images/flag1.png" alt="..."/>
                                                </div>
                                            </div>
                                            <div class="detail">
                                                    <div class="name">
                                                    <div class="left">
                                                        <p>
                                                            Bichon Frise Golden 
                                                        </p>
                                                    </div>
                                                    <div class="right">
                                                        <i class="fal fa-venus"></i>
                                                    </div>
                                                </div>
                                                <div class="loaction">
                                                    <span>
                                                        Dublin, Ireland
                                                    </span>
                                                    <h6>
                                                        £800.00
                                                    </h6>
                                                </div>
                                                <div class="price">
                                                    <div class="visite">
                                                        <a href="#">
                                                            <i class="fal fa-long-arrow-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="one_third match_height_col">
                                            <div class="img_area">
                                                <img src="images/listdog4.png" alt="..." />
                                                <div class="favt">
                                                    <a href="#">
                                                        <i class="fal fa-heart"></i>
                                                    </a>
                                                </div>
                                                <div class="flag">
                                                    <img src="images/flag2.png" alt="..."/>
                                                </div>
                                            </div>
                                            <div class="detail">
                                                    <div class="name">
                                                    <div class="left">
                                                        <p>
                                                            Bichon Frise Golden 
                                                        </p>
                                                    </div>
                                                    <div class="right">
                                                        <i class="fal fa-venus"></i>
                                                    </div>
                                                </div>
                                                <div class="loaction">
                                                    <span>
                                                        Dublin, Ireland
                                                    </span>
                                                    <h6>
                                                        £800.00
                                                    </h6>
                                                </div>
                                                <div class="price">
                                                    <div class="visite">
                                                        <a href="#">
                                                            <i class="fal fa-long-arrow-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>





<!-- ==== footer === -->
<?php include('common/footer.php') ?>
<section class="footer">
	<div class="container">
		<div class="row">
			<div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="footer_area text-center">
					<div class="footer_detail_area">
						<div class="foot_logo">
							<div class="img_area">
								<img src="images/footlogo.png" alt=".." />
							</div>
						</div>
						<p>Connect safely with a good breeder to find a happy healthy puppy </p>
						<div class="cms_links_area">
							<ul>
								<li>
									<a href="javascript:;">About us</a>
								</li>
								<li>
									<a href="javascript:;">Contact us </a>
								</li>
								<li>
									<a href="login.php">Login</a>
								</li>
								<li>
									<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#privacy_policy">Privacy policy </a>
								</li>
								<li>
									<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#terms_conditions">Terms & Conditions </a>
								</li>
								<li>
									<a href="faq.php">FAQs</a>
								</li>
							</ul>
						</div>
						<div class="social_links_area">
							<ul>
								<li>
									<a href="javascript:;"><i class="fab fa-facebook-f"></i></a>
								</li>
								<li>
									<a href="javascript:;"><i class="fab fa-twitter"></i></a>
								</li>
								<li>
									<a href="javascript:;"><i class="fab fa-instagram"></i></a>
								</li>
								<li>
									<a href="javascript:;"><i class="fal fa-envelope"></i></a>
								</li>

							</ul>
						</div>
					</div>
				</div>
				<div class="place_ad">
        <a href="javascript:;">
            <h2>
                Place Ad
            </h2>
        </a>
    </div>
    <div class="place_ad find_dog">
        <a href="javascript:;">
            <h2>
                Find a dog
            </h2>
        </a>
    </div>
			</div>
		</div>
	</div>
</section>

<section class="footer_image_section">
	<div class="image_area">
		<img src="images/footer_bg.png" alt="..." />
		<div class="element">
			<img src="images/element.png" alt="..." />
		</div>
	</div>
</section>

<section class="copyright_section">
	<div class="copyright_area text-center">
		<p>© &nbsp;2022 &nbsp;|&nbsp;All Rights Reserved &nbsp;|&nbsp;Powered By <a href="https://globiztechnology.com/"
		target="_blank">Globiz Technology Inc.</a></p>
	</div>
</section>

<?php include('common/modal.php') ?>

<!-- ==== jQuery JS ==== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<!-- ==== Bootstrap JS ==== -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="plugins/owl2/js/owl.carousel.js"></script>
<script src="plugins/owl2/js/owl-custom.js"></script>
<!-- ==== Slick Carausel Js ==== -->
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<!-- ==== Owl Carausel Js ==== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<!-- ==== jQuery validation JS ==== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>
<!-- ==== jQuery Additional method validation JS ==== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/additional-methods.min.js"></script>
<!-- ==== jQuery matchHeight JS === -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
<!-- ==== Custome JS === -->
<script type="text/javascript" src="js/custom.js"></script>
<!-- ==== Range Plugin JS ==== -->
<script src="https://globizcloudserver.com/globalrental/assets/frontend/plugins/jquery-ui/jquery-ui.js"></script>
<script src="https://globizcloudserver.com/globalrental/assets/frontend/plugins/jquery-ui/jquery.ui.touch-punch.min.js"></script>
</body>
</html>
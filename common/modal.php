<!-- ==== Dashboard change detail  ==== -->

<div class="modal el_modal change_home fade" id="change_detail" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
                <h5 class="modal-title">Change details </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <i class="far fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="credit_detail">
                    <div class="col-xxl-12 col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" name=""
                                placeholder="Enter your name">
                        </div>
                    </div>
                    <div class="col-xxl-12 col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="form-group">
                            <label>Card number</label>
                            <div class="form_icon">
                                <input type="number" class="form-control" name=""
                                    placeholder="Enter your card number">
                                <div class="icon_area">
                                    <i class="far fa-lock"></i>
                                </div>
                            </div>
                            <p>Pay with your MasterCard, Visa, Discover or American Express
                            </p>
                        </div>
                    </div>
                    <div class="col-xxl-6 col-xl-6 col-lg-8 col-md-8 col-sm-6 col-12 ">
                        <div class="form-group">
                            <label>Expiration date</label>
                            <input type="text" class="form-control" name=""
                                placeholder="00/00">
                        </div>
                    </div>
                    <div class="col-xxl-6 col-xl-6 col-lg-8 col-md-8 col-sm-6 col-12 ">
                        <div class="form-group">
                            <label>Security code</label>
                            <div class="form_icon">
                                <input type="text" class="form-control" name=""
                                    placeholder="***">
                                <div class="icon_area">
                                    <i class="far fa-lock"></i>
                                </div>
                            </div>
                            <p>Your information is secure. </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="view">
                    <a href="#" class="btn btn-primary-1">Save changes</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Health Test Modal -->
<div class="modal el_modal health_test fade" id="health_test" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Health Test</h2>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="credit_detail">
                    <form>
                        <div class="row">
                            <div class="col-xxl-12 col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label>Test name</label>
                                    <div class="form_icon">
                                        <input type="number" class="form-control" name=""
                                            placeholder="Enter Test name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label>Result</label>
                                    <div class="form_icon">
                                        <input type="number" class="form-control" name=""
                                            placeholder="Enter Result">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">
                                <h5>Test Date</h5>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="form-group">
                                    <label>Day</label>
                                    <div class="form_icon">
                                    <select class="form-select">
                                        <option>Day</option>
                                        <option value="">one</option>
                                        <option value="">two</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="form-group">
                                    <label>Month</label>
                                    <div class="form_icon">
                                    <select class="form-select">
                                        <option>Month</option>
                                        <option value="">one</option>
                                        <option value="">two</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="form-group">
                                    <label>Year</label>
                                    <div class="form_icon">
                                    <select class="form-select">
                                        <option>Year</option>
                                        <option value="">one</option>
                                        <option value="">two</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <p>Certificate</p>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="icon_add">
                                    <label for="file-input">
                                        <h5><span><i class="fal fa-cloud-upload"></i></span>Upload file</h5>
                                    </label>
                                    <input id="file-input" class="d-none" type="file" />
                                </div>
                            </div>  
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="button">
                                    <a href="#" class="btn btn-primary-1">Upload</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Health Test_2 Modal -->
<div class="modal el_modal health_test fade" id="health_test2" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Add/Edit/View Health Test</h2>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="credit_detail">
                    <form>
                        <div class="row">
                            <div class="col-xxl-12 col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label>Test name</label>
                                    <div class="form_icon">
                                        <input type="number" class="form-control" name=""
                                            placeholder="Enter Test name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label>Result</label>
                                    <div class="form_icon">
                                        <input type="number" class="form-control" name=""
                                            placeholder="Enter Result">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">
                                <h5>Test Date</h5>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="form-group">
                                    <label>Day</label>
                                    <div class="form_icon">
                                    <select class="form-select">
                                        <option>Day</option>
                                        <option value="">one</option>
                                        <option value="">two</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="form-group">
                                    <label>Month</label>
                                    <div class="form_icon">
                                    <select class="form-select">
                                        <option>Month</option>
                                        <option value="">one</option>
                                        <option value="">two</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="form-group">
                                    <label>Year</label>
                                    <div class="form_icon">
                                    <select class="form-select">
                                        <option>Year</option>
                                        <option value="">one</option>
                                        <option value="">two</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <p>Certificate</p>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="icon_add">
                                    <label for="file-input">
                                        <h5><span><i class="fal fa-cloud-upload"></i></span>Upload file</h5>
                                    </label>
                                    <input id="file-input" class="d-none" type="file" />
                                </div>
                            </div>  
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="button">
                                    <a href="#" class="btn btn-primary-1">Upload</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Privacy_policy Modal -->
<div class="modal el_modal privacy_policy fade" id="privacy_policy" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Privacy policy</h2>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="content_box">
                            <div class="content">
	                            <h1><img src="https://globizcloudserver.com/browsepets/public/uploads/editor/16564885497904-l-16504445639213-foun.png" style="max-width: 100%; max-height: 400px;"></h1>

                                <h1><strong>Privacy Policy</strong></h1>

                                <p>This document is an electronic record in terms of Information Technology Act, 2000 and rules there under as applicable and the provisions pertaining to electronic records in various statutes as amended by the Information Technology Act, 2000. This electronic record is generated by a computer system and does not require any physical or digital signatures to make the terms of this policy binding.</p>

                                <p>At Browse4Pets, data protection is a matter of trust and your privacy is very important to us. We use your personal information only in the manner set out in this Privacy Policy. Please read this privacy policy to learn more about our information gathering and dissemination practices.</p>

                                <p>This Privacy Policy explains how we gather personal information, classified as mandatory or optional as part of the normal operation of our services; and use, disclose and protect such information through the Site. This detailed privacy policy enables you to take informed decisions in dealings with us.</p>

                                <p>By availing our services, you acknowledge your acceptance of the terms of this Privacy Policy, expressly consent to our use and disclosure of your personal information in accordance with this Privacy Policy and that any personal information provided by you through us is provided under a lawful contract. This Privacy Policy is incorporated into and subject to the terms of the User Agreement and/or all other policies and agreements published on the website.</p>

                                <p>We are extremely proud of our commitment to protect your privacy. We value your trust in us. We work hard to earn your confidence so that you can enthusiastically use our services and recommend your friends and family to participate in dealing with us. Please read the following policy to understand how your personal information will be treated as you make full use of our Site.</p>

                                <p>Our privacy policy is subject to change at any time without notice. To make sure you are aware of any changes, please review this policy periodically.</p>

                                <p>&nbsp;</p>

                                <p><strong>1.&nbsp;<u>INFORMATION WE COLLECT:</u></strong></p>

                                <p>You provide us information about yourself – your name, address, e–mail, phone number, date of birth, etc, in case of registration using social networking websites we collect your profile picture and social network ID. when you interact with us for availing our services. If you correspond with us by e-mail, we may retain the content of your e-mail messages, your e-mail address, and our responses. Additionally, we store information about users’ contacts when users manually enter contact e-mail addresses or transfer contact information from other online social networks. We also collect general information about your use of our services.</p>

                                <p>&nbsp;</p>

                                <p><strong>2.&nbsp;<u>INFORMATION WE COLLECT AUTOMATICALLY WHEN YOU USE OUR SERVICES:</u></strong></p>

                                <p>When you access or use our Services, we automatically collect information about you, including:</p>

                                <ol>
                                    <li>Device Information: We may collect information about the device you use to access our Services, including the hardware model, operating system and version, unique device identifier, phone number, International Mobile Equipment Identity ("IMEI") and mobile network information.</li>
                                    <li>Information Collected by Cookies and Other Tracking Technologies: We use various technologies to collect information, and this may include sending cookies to you. A "cookie" is a small data file transferred to your computer’s hard drive that allows a Website to respond to you as an individual, gathering and remembering information about your preferences in order to tailor its operation to your needs, likes and dislikes. Overall, cookies are safe, as they only identify your computer to customize your Web experience. Accepting a cookie does not provide us access to your computer or any Personally Identifiable Information about you, other than the information you choose to share. Other servers cannot read them, nor can they be used to deliver a virus. Most browsers automatically accept cookies, but you can usually adjust yours (Microsoft Internet Explorer, Firefox or Google Chrome) to notify you of cookie placement requests, refuse certain cookies, or decline cookies completely. If you turn off cookies completely, there may be some website features that will not be available to you, and some Web pages may not display properly. To support the personalized features of our website (such as your country and language codes and browsing functions) we must send a cookie to your computer’s hard drive and/or use cookie-based authentication to identify you as a registered website user. We do not, however, use so-called "surveillance" cookies that track your activity elsewhere on the Web. We may also collect information using web beacons (also known as "tracking pixels").</li>
                                    <li>We also may make use of third party tracking pixels used by advertising or analytical partners. Some such partners include, but are not limited to:</li>
                                </ol>

                                <ol>
                                    <li>Google Analytics: Used to track statistical information such as page visits and traffic source information allowing us to improve the performance and quality of the Site. For more information please visit:&nbsp;<a href="http://www.google.com/analytics/learn/privacy.html">http://www.google.com/analytics/learn/privacy.html</a>.</li>
                                    <li>Google Advertising: Used to track conversions from advertisements on the Google Search and Google Display network. For more information please visit:&nbsp;<a href="http://www.google.com/policies/technologies/ads/">http://www.google.com/policies/technologies/ads/</a>. Third party pixels and content may make use of cookies. We do not have access or control over these third party cookies and this Policy does not cover the use of third party cookies.</li>
                                </ol>

                                <p>&nbsp;</p>

                                <p><strong>3.&nbsp;<u>INFORMATION SHARED DURING TRANSACTION:</u></strong></p>

                                <ol>
                                    <li>You agree that you will enter into transactions with third parties through our website and will share your personally identifiable information with them for easy completion of the transaction. You hereby expressly agree that we shall not be involved or held liable for any breach of the privacy or security of that data. The said breach, if any, shall be a matter of dispute between you and the third party and we shall not be held liable or be issued a notice for the same.&nbsp;</li>
                                    <li>We STRONGLY recommend that you should be careful and vigilant while disclosing your personally identifiable details with your transaction partner. Please do not disclose your bank and account details to anyone through our website or to any individual whom you have met through our website.&nbsp;</li>
                                </ol>

                                <p>&nbsp;</p>

                                <p><strong>4.&nbsp;<u>HOW WE USE YOUR INFORMATION:</u></strong></p>

                                <ol>
                                    <li>We use the personal information we collect to fulfill your requests for services, improve our services, contact you, conduct research, and provide our other users with your information to ascertain compatibility.</li>
                                    <li>By providing us your e-mail address, you consent to us using the e-mail address to send you, our website and services related notices, including any notices required by law, in lieu of communication by postal mail. You also agree that we may send notifications of activity on our website to the e-mail address you give us, in accordance with any applicable privacy settings. We may use your e-mail address to send you other messages, such as newsletters, changes to our features, new services, events or other information. If you do not want to receive optional e-mail messages, you may modify your settings to opt out.</li>
                                    <li>Our settings may also allow you to adjust your communications preferences. If you do not wish to receive promotional email messages from us, you may opt out by following the unsubscribe instructions in those emails. If you opt out, you will still receive non-promotional emails from us about enquiring of any additional information that we may require.</li>
                                    <li>Following termination or deactivation of your services, we may (but are under no obligation to) retain your information for archival purposes. We will not publicly disclose any of your personally identifiable information other than as described in this Privacy Policy.</li>
                                </ol>

                                <p>&nbsp;</p>

                                <p><strong>5.&nbsp;<u>HOW WE SHARE YOUR INFORMATION:</u></strong></p>

                                <ol>
                                    <li>As a matter of policy, we do not sell or rent information about you and we do not disclose information about you in a manner inconsistent with this Privacy Policy except as required by law or government regulation. We cooperate with law enforcement inquiries, as well as other third parties, to enforce laws such as those regarding, fraud and other personal rights.</li>
                                    <li>We will not share the personal information we collect about you with any third party for marketing their own services only when approved by you.&nbsp;We may share your data with our services providers who process your personal information to provide services to us or on our behalf.&nbsp; We have contracts with our service providers that prohibit them from sharing the information about you that they collect or that we provide to them with anyone else, or using it for other purposes.</li>
                                    <li>You may decline to submit Personally Information through the Services, in which case we may not be able to provide certain services to you. If you do not agree with our Privacy Policy or Terms of Service, please discontinue use of our Service; your continued usage of the Service will signify your assent to and acceptance of our Privacy Policy and Terms of Use.</li>
                                    <li>WE CAN (AND YOU AUTHORIZE US TO) DISCLOSE ANY INFORMATION ABOUT YOU TO LAW ENFORCEMENT, OTHER GOVERNMENT OFFICIALS, ANY LAWSUIT OR ANY OTHER THIRD PARTY THAT WE, IN OUR SOLE DISCRETION, BELIEVE NECESSARY OR APPROPRIATE IN CONNECTION WITH AN INVESTIGATION OF FRAUD, INTELLECTUAL PROPERTY INFRINGEMENT, OR OTHER ACTIVITY THAT IS ILLEGAL OR MAY EXPOSE US, OR YOU, TO LIABILITY.</li>
                                </ol>

                                <p>&nbsp;</p>

                                <p><strong>6.&nbsp;<u>ENSURING INFORMATION IS ACCURATE AND UP-TO-DATE</u></strong></p>

                                <ol>
                                    <li>We take reasonable precautions to ensure that the Personal Information We Collect, Use and Disclose is complete, relevant and up-to-date. However, the accuracy of that information depends to a large extent on the information you provide. That's why we recommend that you:</li>
                                </ol>

                                <ol>
                                    <li>Let us know if there are any errors in your Personal Information; and</li>
                                    <li>Keep us up-to-date with changes to your Personal Information such as your name or address.</li>
                                </ol>

                                <p>&nbsp;</p>

                                <p><strong>7.&nbsp;<u>HOW WE PROTECT YOUR INFORMATION:</u></strong></p>

                                <ol>
                                    <li>We are very concerned about safeguarding the confidentiality of your personally identifiable information. We employ administrative, physical and electronic measures designed to protect your information from unauthorized access.</li>
                                    <li>By using this website or the Services or providing Personal Information to us, you agree that we can communicate with you electronically regarding security, privacy, and administrative issues relating to your use of this website or Services.</li>
                                    <li>We use commercially reasonable physical, managerial, and technical safeguards to preserve the integrity and security of your Personal Information. Your sensitive information, such as credit card information, is encrypted when it is transmitted to us. Our employees are trained and required to safeguard your information. We cannot, however, ensure or warrant the security of any information you transmit to us and you do so at your own risk. Using unsecured wi-fi or other unprotected networks to submit messages through the Service is never recommended. Once we receive your transmission of information, we make commercially reasonable efforts to ensure the security of our systems. However, please note that this is not a guarantee that such information may not be accessed, disclosed, altered, or destroyed by breach of any of our physical, technical, or managerial safeguards. If we learn of a security systems breach, then we may attempt to notify you electronically so that you can take appropriate protective steps.</li>
                                    <li>Notwithstanding anything to the contrary in this Policy, we may preserve or disclose your information if we believe that it is reasonably necessary to comply with a law, regulation or legal request; to protect the safety of any person; to address fraud, security or technical issues; or to protect our rights or property. However, nothing in this Policy is intended to limit any legal defenses or objections that you may have to a third party, including a government’s, request to disclose your information.</li>
                                    <li>However, no data transmission over the Internet or data storage system can be guaranteed to be 100% secure. Please do not send us credit card information and/or other sensitive information through email. If you have reason to believe that your interaction with us is not secure (for example, if you feel that the security of any account you might have with us has been compromised), you must immediately notify us of the problem by contacting us in accordance with the "Contacting Us" section available on our website.</li>
                                </ol>

                                <p>&nbsp;</p>

                                <p><strong>8.&nbsp;</strong><strong><u>YOUR CHOICES ABOUT YOUR INFORMATION:</u></strong></p>

                                <ol>
                                    <li>You have several choices regarding use of information on our Services:</li>
                                    <li>Email Communications: We may periodically send you free newsletters and e- mails that directly promote the use of our website, or Services. When you receive newsletters or promotional communications from us, you may indicate a preference to stop receiving further communications from us and you will have the opportunity to “opt-out” by following the unsubscribe instructions provided in the e-mail you receive or by contacting us directly. Despite your indicated e-mail preferences, we may send you service related communications, including notices of any updates to our Terms of Use or Privacy Policy.</li>
                                    <li>Changing or Deleting Your Personal Information: You may change any of your personal information in your Account by emailing us at&nbsp;<a href="mailto:support@only4pets.com"><strong>SUPPORT</strong></a>&nbsp;or by visiting the website and following the directions therein. You may request deletion of your personal information by us, and we will use commercially reasonable efforts to honor your request, but please note that we may be required to keep such information and not delete it (or to keep this information for a certain time, in which case we will comply with your deletion request only after we have fulfilled such requirements). When we delete any information, it will be deleted from the active database, but may remain in our archives. We may also retain your information for fraud prevention or similar purposes.</li>
                                </ol>

                                <ol>
                                    <li>You may, of course, decline to submit personally identifiable information through the website, in which case you may provide them to us by e-mail or post. You can review and correct the information about you, that we keep on file by communicating with us and we shall make the required edits by contacting us directly at&nbsp;<strong><a href="mailto:support@only4pets.com">SUPPORT</a></strong>.</li>
                                </ol>

                                <p>&nbsp;</p>

                                <p><strong>9.&nbsp;<u>CHILDREN’S PRIVACY:</u></strong></p>

                                <p>Our services are available only to persons who can form a legally binding contract under the Indian Contract Act, 1872. Protecting the privacy of young children is especially important. Thus, we do not knowingly collect or solicit personal information from anyone under the age of 18 or knowingly allow such persons to register. If you are under 18, please do not attempt to register for the Service or send any information about yourself to us, including your name, address, telephone number, or email address. No one under the age of 18 may provide any personal information for accessing the Service. In the event that we learn that we have collected personal information from a child under age 18, we will delete that information as quickly as possible. If you believe that we might have any information from or about a child under 18, please contact us through our privacy help page.</p>

                                <p>&nbsp;</p>

                                <p><strong>10.<u>&nbsp;MERGER AND ACQUISITIONS:</u></strong></p>

                                <p>In case of a merger or acquisition, we reserve the right to transfer all the information, including personally identifiable information, stored with us to the new entity or company thus formed. Any change in the website’s policies and standing will be notified to you through email.</p>

                                <p>&nbsp;</p>

                                <p><strong>11.&nbsp;<u>ADVERTISING:</u></strong></p>

                                <p>Advertisers and advertising networks place ads (including sponsored links in search results) on our website. These companies, as well as data analytics companies who service them, may use cookies, pixel tags, mobile device IDs and other similar technologies to collect data about you when you visit our website.&nbsp; They use the data to enable them to track your activity across various sites where they display ads and record your activities, so they can show ads that they consider relevant to you.&nbsp; You may opt-out of receiving targeted online advertising from advertising networks that is delivered on our website.</p>

                                <p>&nbsp;</p>

                                <p><strong>12.&nbsp;<u>GOOGLE:</u></strong></p>

                                <ol>
                                    <li>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users.&nbsp;<a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en">https://support.google.com/adwordspolicy/answer/1316548?hl=en</a>. Our website uses Google AdSense Advertising.</li>
                                    <li>Google, as a third party vendor, uses cookies to serve ads on our website Google’s use of the DART cookie enables it to serve ads to our website users based on their visit to our website and other sites on the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy.</li>
                                    <li>Our website has implemented the following: -</li>
                                </ol>
                                <ul>
                                    <li>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users.&nbsp;<a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en">https://support.google.com/adwordspolicy/answer/1316548?hl=en</a>. Our website uses Google AdSense Advertising.</li>
                                    <li>Google, as a third party vendor, uses cookies to serve ads on our website Google’s use of the DART cookie enables it to serve ads to our website users based on their visit to our website and other sites on the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy.</li>
                                    <li>Our website has implemented the following: -</li>
                                </ul>
                                <ul>
                                    <li>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users.&nbsp;<a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en">https://support.google.com/adwordspolicy/answer/1316548?hl=en</a>. Our website uses Google AdSense Advertising.</li>
                                    <li>Google, as a third party vendor, uses cookies to serve ads on our website Google’s use of the DART cookie enables it to serve ads to our website users based on their visit to our website and other sites on the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy.</li>
                                    <li>Our website has implemented the following: -</li>
                                </ul><ul>
                                    <li>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users.&nbsp;<a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en">https://support.google.com/adwordspolicy/answer/1316548?hl=en</a>. Our website uses Google AdSense Advertising.</li>
                                    <li>Google, as a third party vendor, uses cookies to serve ads on our website Google’s use of the DART cookie enables it to serve ads to our website users based on their visit to our website and other sites on the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy.</li>
                                    <li>Our website has implemented the following: -</li>
                                </ul><ul>
                                    <li>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users.&nbsp;<a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en">https://support.google.com/adwordspolicy/answer/1316548?hl=en</a>. Our website uses Google AdSense Advertising.</li>
                                    <li>Google, as a third party vendor, uses cookies to serve ads on our website Google’s use of the DART cookie enables it to serve ads to our website users based on their visit to our website and other sites on the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy.</li>
                                    <li>Our website has implemented the following: -</li>
                                </ul><ul>
                                    <li>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users.&nbsp;<a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en">https://support.google.com/adwordspolicy/answer/1316548?hl=en</a>. Our website uses Google AdSense Advertising.</li>
                                    <li>Google, as a third party vendor, uses cookies to serve ads on our website Google’s use of the DART cookie enables it to serve ads to our website users based on their visit to our website and other sites on the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy.</li>
                                    <li>Our website has implemented the following: -</li>
                                </ul><ul>
                                    <li>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users.&nbsp;<a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en">https://support.google.com/adwordspolicy/answer/1316548?hl=en</a>. Our website uses Google AdSense Advertising.</li>
                                    <li>Google, as a third party vendor, uses cookies to serve ads on our website Google’s use of the DART cookie enables it to serve ads to our website users based on their visit to our website and other sites on the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy.</li>
                                    <li>Our website has implemented the following: -</li>
                                </ul><ul>
                                    <li>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users.&nbsp;<a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en">https://support.google.com/adwordspolicy/answer/1316548?hl=en</a>. Our website uses Google AdSense Advertising.</li>
                                    <li>Google, as a third party vendor, uses cookies to serve ads on our website Google’s use of the DART cookie enables it to serve ads to our website users based on their visit to our website and other sites on the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy.</li>
                                    <li>Our website has implemented the following: -</li>
                                </ul>


                                <ol>
                                    <li>Remarketing with Google AdSense</li>
                                    <li>Google Display Network Impression Reporting</li>
                                    <li>Demographics and Interests Reporting</li>
                                    <li>DoubleClick Platform Integration</li>
                                </ol>

                                <ol>
                                    <li>Our website along with third-party vendors, such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user’s interactions with ad impressions, and other ad service functions as they relate to our website.</li>
                                </ol>

                                <p>&nbsp;</p>

                                <p><strong>13.&nbsp;<u>NOTIFICATION PROCEDURES:</u></strong></p>

                                <p>It is our policy to provide notifications, whether such notifications are required by law or are for marketing or other business related purposes, to you via e-mail notice, written or hard copy notice, or through conspicuous posting of such notice on the website, as determined by us in our sole discretion. We reserve the right to determine the form and means of providing notifications to you, provided that you may opt out of certain means of notification as described in this Privacy Policy.</p>

                                <p>&nbsp;</p>

                                <p><strong>14.&nbsp;<u>SURVEYS:</u></strong></p>

                                <p>We also use surveys to collect information about our users. From time to time, we request users' input in order to evaluate potential features and services. The decision to answer a survey is completely yours. We use information gathered from surveys to improve our services.</p>

                                <p>&nbsp;</p>

                                <p><strong>15.&nbsp;<u>OPTING OUT OF INFORMATION SHARING:</u></strong></p>

                                <ol>
                                    <li>We understand and respect that not all users may want to allow us to share their information with third parties. If you do not want us to share your information, please contact us through the Contact Us page, and we will remove your name from lists we share with third parties as soon as reasonably practicable or you can also click on unsubscribe. When contacting us, please clearly state your request, including your name, mailing address, email address and phone number.</li>
                                    <li>However, under the following circumstances, we may still be required to share your personal information:</li>
                                </ol>

                                <ol>
                                    <li>If we respond to court orders or legal process, or if we need to establish or exercise our legal rights or defend against legal claims.</li>
                                    <li>If we believe it is necessary to share information in order to investigate, prevent or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of our Terms of Use or as otherwise required by law.</li>
                                    <li>If we believe it is necessary to restrict or inhibit any user from using any of our Site, including, without limitation, by means of "hacking" or defacing any portion thereof.</li>
                                </ol>

                                <p>&nbsp;</p>

                                <p><strong>16.&nbsp;<u>PHISHING OR FALSE EMAILS</u></strong><strong><u>:</u></strong></p>

                                <p>If you receive an unsolicited email that appears to be from us or one of our members that requests personal information (such as your credit card, login, or password), or that asks you to verify or confirm your account or other personal information by clicking on a link, that email was likely to have been sent by someone trying to unlawfully obtain your information, sometimes referred to as a "phisher" or "spoofer." We do not ask for this type of information in an email. Do not provide the information or click on the link.&nbsp; Please contact us at <a href="https://globizcloudserver.com/browsepets/public/contact-us"><b>Get in Touch</b></a>&nbsp;if you get an email like this.</p>

                                <p>&nbsp;</p>

                                <p><strong>17.&nbsp;<u>CHANGES TO OUR PRIVACY POLICY:</u></strong></p>

                                <ol>
                                    <li>We may update this Privacy Policy and information security procedures from time to time. If these privacy and/or information security procedures materially change at any time in the future, we will post the new changes conspicuously on the website to notify you and provide you with the ability to opt out in accordance with the provisions set forth above.</li>
                                    <li>Continued use of our website and Service, following notice of such changes shall indicate your acknowledgement of such changes and agreement to be bound by the terms and conditions of such changes.</li>
                                </ol>

                                <p>&nbsp;</p>

                                <p><strong>18.&nbsp;<u>BREACH OF PRIVACY POLICY:</u></strong></p>

                                <p>We reserve the right to terminate or suspend your usage of this website immediately if you are found to be in violation of our privacy policy. We sincerely request you to respect privacy and secrecy concerns of others. The jurisdiction of any breach or dispute shall be&nbsp;<strong><u>Indore, M.P, India</u></strong>.</p>

                                <p>&nbsp;</p>

                                <p><strong>19.&nbsp;<u>NO RESERVATIONS:</u></strong></p>

                                <p>We do not accept any reservation or any type of limited acceptance of our privacy policy. You expressly agree to each and every term and condition as stipulated in this policy without any exception whatsoever.</p>

                                <p>&nbsp;</p>

                                <p><strong>20.&nbsp;<u>NO CONFLICT:</u></strong></p>

                                <p>The policy constitutes a part of the user terms. We have taken utmost care to avoid any inconsistency or conflict of this policy with any other terms, agreements or guidelines available on our website. In case there exists a conflict, we request you to kindly contact us for the final provision and interpretation.</p>

                                <p>&nbsp;</p>

                                <p><strong>21.&nbsp;<u>APPLICABILITY</u></strong></p>

                                <p>This privacy policy is applicable to both the mobile app and the website of <a href="https://globizcloudserver.com/browsepets/public/">Browse4Pets</a><strong>.</strong><strong>&nbsp;</strong></p>

                                <p>&nbsp;</p>
                                <p><strong>22.&nbsp;<u>CONTACT US:</u></strong></p>
                                <p>If you have any questions about this Privacy Policy, our practices relating to the website, or your dealings with us, please contact the Privacy Team at: <a href="https://globizcloudserver.com/browsepets/public/contact-us"><b>Contact us</b></a>.</p>
	                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal el_modal privacy_policy fade" id="terms_conditions" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Terms & Conditions</h2>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="content_box">
                            <div class="content">
                                <h6>Nullam interdum lorem ipsumt dor aliquam lobortis dolor</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum 
                                    suscipit quam non eleifend ornare. Nullam porta volutpat facilisis. 
                                    Mauris vehicula nisl eget elit aliquet, id dapibus odio iaculis. 
                                    Duis id consectetur mi. Vivamus aliquam lorem lacus, ac molestie sapien 
                                    sagittis eget. Cras eget elementum ligula. Donec efficitur quam ut 
                                    tortor pretium molestie. Fusce velit purus, cursus at risus non, laoreet tempus diam. </p>
                            </div>
                            <div class="content">
                                <h6>lorem ipsum sit dor aliquam dolor</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum 
                                    suscipit quam non eleifend ornare. Nullam porta volutpat facilisis. 
                                    Mauris vehicula nisl eget elit aliquet, id dapibus odio iaculis. 
                                    Duis id consectetur mi. Vivamus aliquam lorem lacus, ac molestie sapien 
                                    sagittis eget. Cras eget elementum ligula. Donec efficitur quam ut 
                                    tortor pretium molestie. Fusce velit purus, cursus at risus non, laoreet tempus diam. </p>
                            </div>
                            <div class="content">
                                <h6>lorem ipsum sit dor aliquam dolor</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum 
                                    suscipit quam non eleifend ornare. Nullam porta volutpat facilisis. 
                                    Mauris vehicula nisl eget elit aliquet, id dapibus odio iaculis. 
                                    Duis id consectetur mi. Vivamus aliquam lorem lacus, ac molestie sapien 
                                    sagittis eget. Cras eget elementum ligula. Donec efficitur quam ut 
                                    tortor pretium molestie. Fusce velit purus, cursus at risus non, laoreet tempus diam. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Get notified Modal -->
<div class="modal el_modal get_notified fade" id="get_notified" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Get Notified</h2>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="inner_area">
                    <form>
                        <div class="row">
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <p>Breed</p>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <h6>You can select upto 5 breeds</h6>
                            </div>
                            <div class="col-xxl-12 col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="accordion" id="accordionExample">
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="headingOne">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Select
                                        </button>
                                        </h2>
                                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                            <div class="accordion-body">
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="l-1" name="district[]" value="1" />
                                                        <label class="custom-control-label" for="l-1">Ankita</label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="l-2" name="district[]" value="1" checked="" />
                                                        <label class="custom-control-label" for="l-2">Basenji</label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="l-3" name="district[]" value="1" />
                                                        <label class="custom-control-label" for="l-3">Bulldog</label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="l-4" name="district[]" value="1" />
                                                        <label class="custom-control-label" for="l-4">Bearded Collie</label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="l-5" name="district[]" value="1" />
                                                        <label class="custom-control-label" for="l-5">Cairn Terrier</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="button">
                                    <a href="javascript:;" class="btn btn-primary-2">Notify me</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add dog Modal -->
<div class="modal el_modal add_dog fade" id="add_dog" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Add Dog</h2>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="inner_area">
                    <form>
                        <div class="row">
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="microchip_number">Microchip Number</label>
                                    <input type="text" class="form-control" placeholder="Microchip Number" autocomplete="off" />
                                </div>   
                            </div>
                            
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="gender">Gender</label>
                                    <select class="form-select">
                                        <option value="">Select</option>
                                        <option value="">Male</option>
                                        <option value="">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="color">Color</label>
                                    <select class="form-select">
                                        <option value="">Select</option>
                                        <option value="">Red</option>
                                        <option value="">Green</option>
                                        <option value="">Blue</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="button">
                                    <a href="javascript:;" class="btn btn-primary-2">Add Dog</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Get subscriber plan Modal -->
<div class="modal el_modal get_subscriber_plan fade" id="get_subscriber_plan" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="inner_area">
                    <form>
                        <div class="row">
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="image">
                                    <img src="images/notific.png" alt="..." />
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="content">
                                    <p>You can only view these options if you are an active subscriber</p>
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="button">
                                    <a href="javascript:;" class="btn btn-primary">Okay</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Get notified Modal -->
<div class="modal el_modal instruction_tips fade" id="instruction_tips" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Instruction and Tips</h2>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="inner_area">
                    <form>
                        <div class="row">
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum suscipit quam non eleifend ornare. 
                                    Nullam porta volutpat facilisis. Mauris vehicula nisl eget elit aliquet, id dapibus odio iaculis. 
                                    Duis id consectetur mi. Vivamus aliquam lorem lacus, ac molestie sapien sagittis eget. Cras eget elementum ligula. 
                                    Donec efficitur quam ut tortor pretium molestie. Fusce velit purus, cursus at risus non, laoreet tempus diam. 
                                </p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum suscipit quam non eleifend ornare. 
                                    Nullam porta volutpat facilisis. Mauris vehicula nisl eget elit aliquet, id dapibus odio iaculis. 
                                    Duis id consectetur mi. Vivamus aliquam lorem lacus, ac molestie sapien sagittis eget. Cras eget elementum ligula. 
                                    Donec efficitur quam ut tortor pretium molestie. Fusce velit purus, cursus at risus non, laoreet tempus diam. 
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
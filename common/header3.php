<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Browse4pets</title>

	<link rel="icon" href="images/logo.png" type="image/x-icon" />
	<!-- ==== Bootstrap CSS ==== -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.1.3/css/bootstrap.min.css" integrity="sha512-GQGU0fMMi238uA+a/bdWJfpUGKUkBdgfFdgBm72SUQ6BeyWjoY/ton0tEjH+OSH9iP4Dfh+7HM0I9f5eR0L/4w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<!-- ==== Montserrat Fonts ==== -->
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet" />
	<!-- ==== Font Awesome CSS Linear Flat Icons ==== -->
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.4/css/all.css" />
	<link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
	<link rel='stylesheet' href='https://cdn-uicons.flaticon.com/uicons-regular-rounded/css/uicons-regular-rounded.css'>
	<link rel='stylesheet' href='https://cdn-uicons.flaticon.com/uicons-solid-rounded/css/uicons-solid-rounded.css'>
	<!-- ==== Owl Carausel CSS ==== -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css" integrity="sha512-UTNP5BXLIptsaj5WdKFrkFov94lDx+eBvbKyoe1YAfjeRPC+gT5kyZ10kOHCfNZqEui1sxmqvodNUx3KbuYI/A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<!-- Slick slider -->
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
	<!-- ==== Select 2 ==== -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />

	<!-- ==== Custom font ==== -->
	<link rel="stylesheet" href="fonts/stylesheet.css" />
	
	<!-- ==== Custom CSS ==== -->
	<link rel="stylesheet" href="css/custom.css" />
	<!-- ==== Range Plugin CSS ==== -->
	<link rel="stylesheet" href="https://globizcloudserver.com/globalrental/assets/frontend/plugins/jquery-ui/jquery-ui.css" type="text/css"/>
</head>
<body>

<section class="header-main-section ">
	<div class="header_menu_wraper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="head_navigation d-lg-block d-none">
						<div class="left_area ">
							<div class="logo">
								<a href="index.php">
									<img src="images/headlogo.png" alt="..." />
								</a>
							</div>
							<div class="menus">
								<ul>
									<li>
										<a href="#">Find a dog</a>
									</li>
									<!-- <li>
										<a href="#">Place ad</a>
									</li> -->
									<li>
										<a href="#">Breeders</a>
									</li>
									<li>
										<a href="#">Contact Us </a>
									</li>
								</ul>
							</div>
						</div>
						<div class="right_area">
							<div class="login_side">
								<ul class="home-side">
									<li>
										<a href="javascript:;">  <img src="images/notifications.png" alt="..." /> </a>
										<!-- <div class="dot"></div> -->
									</li>
									<!-- <li class="search_li">
										<a href="login.php" class="btn btn-primary-2">Login</a>
									</li>
									<li class="search_li">
										<a href="signup.php" class="btn btn-primary">Register</a>
									</li> -->
									<li class="user">
										<div class="dropdown user">
											<button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
											<div class="user-img">
												<img src="images/profile.png" alt="..." />
											</div>
											</button>
											<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
												<li><a class="dropdown-item" href="dashboard_home.php"><i class="fal fa-home-alt"></i>My Account</a></li>
												<li><a class="dropdown-item" href="dashboard_setting.php"><i class="fal fa-cog"></i>Settings</a></li>
												<li><a class="dropdown-item" href="index.php"><i class="fal fa-sign-out"></i>Logout</a></li>
											</ul>
										</div>
									</li>
									<li class="search_li">
										<a href="signup.php" class="btn btn-primary">Place Ad</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="responsive_menu  d-lg-none d-block">
						<div class="menu_boxes">
							<div class="logo">
								<div class="img_logo">
									<a href="index.php">
										<img src="images/headlogo.png" alt="..." />
									</a>
								</div>
							</div>
							<div class="right">
								<div class="cart">
									<a href="javascript:;"><i class="fi fi-rr-shopping-cart"></i></a>
								</div>
								<div class="open_close">
									<a href="javascript:;" class="res_menubar">
										<i class="fas fa-bars"></i>
									</a>
								</div>
							</div>
						</div>
						<div class="open_menu">
							<div class="top_area">
								<div class="logo_box">
									<a href="index.php">
										<img src="images/headlogo.png" alt="..." />
									</a>
								</div>
								<div class="cross_menu">
									<a href="javascript:;">
										<i class="far fa-times"></i>
									</a>
								</div>
							</div>
							<div class="inner_res_menu inner_sidebar">
								<ul class="ul_first">
									<li>
										<a href="dashboard_profile.php">My Profile</a>
									</li>
									<li>
										<a href="dashboard_favourites.php">My Favourites</a>
									</li>
									<li>
										<a href="#">Messages</a>
									</li>
									<li>
										<a href="dashboard_change_password.php">Change Password</a>
									</li>
									
									<li>
										<a href="#">Logout</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
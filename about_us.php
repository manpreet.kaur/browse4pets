<!-- ==== Header_section start === -->
<?php include('common/header.php') ?>
<section class="breadcame_section top-space">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="banner_area">
                    <div class="inner_area">
                        <h2>About Us</h2>
                        <div class="header_img d-lg-block d-none">
                            <img src="images/Vector6.png" alt="..." />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Header_section end === -->

<!-- ==== About_section_start ==== -->
<section class="about_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="row">
                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="content_area">
                            <div class="head">
                                <h2>
                                    Who we are    
                                </h2>
                            </div>
                            <div class="content">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim 
                                    ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
                                    aliquip ex ea commodo consequat.Lorem ipsum dolor sit amet, consectetur 
                                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
                                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.
                                </p>
                                <div class="button">
                                    <a href="javascript:;" class="btn btn-primary-2">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="image_area">
                            <img src="images/cartoon.png" alt="..." />
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</section>
<!-- ==== About_section_end ==== -->

<!-- ==== Values_section_start ==== -->
<section class="values_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="content">
                    <div class="header">
                        <h2>Our Values</h2>
                    </div>
                    <div class="row">
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                            <div class="content_box">
                                <div class="image">
                                    <img src="images/value_1.png" alt="..." />
                                </div>
                                <div class="content_area">
                                    <h2>Lets make change</h2>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem 
                                        accusantium doloremque laudantium, totam rem aperiamSed ut perspiciatis 
                                        unde omnis iste natus error sit voluptatem.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                            <div class="content_box">
                                <div class="image">
                                    <img src="images/value_2.png" alt="..." />
                                </div>
                                <div class="content_area">
                                    <h2>Caring breeders</h2>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem 
                                        accusantium doloremque laudantium, totam rem aperiamSed ut perspiciatis 
                                        unde omnis iste natus error sit voluptatem.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                            <div class="content_box">
                                <div class="image">
                                    <img src="images/value_3.png" alt="..." />
                                </div>
                                <div class="content_area">
                                    <h2>Vet approved</h2>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem 
                                        accusantium doloremque laudantium, totam rem aperiamSed ut perspiciatis 
                                        unde omnis iste natus error sit voluptatem.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Values_section_end ==== -->

<!-- ==== Objective_section_start ==== -->
<section class="objective_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="content">
                    <div class="header_area">
                        <h2>Our Long-Term <span>Objective</span> Every <span>Dog/Puppy</span> Deserves a Loving <span>Home!</span></h2>
                    </div>
                    <div class="content_area">
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti 
                            atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, 
                            similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum 
                            quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio 
                            cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est,At 
                            vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti 
                            atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditat.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Objective_section_end ==== -->

<!-- ==== Clients_section_start ==== -->
<section class="clients_section">
    <div class="circle_1">
        <div class="content_box box_1">
            <h6>23,000</h6>
            <p>Dog Listed</p>
        </div>
        <div class="content_box box_2 happy">
            <h6>10,000</h6>
            <p>Happy Clients</p>
        </div>
        <div class="content_box box_3">
            <h6>18,000</h6>
            <p>Lorem Ipsum</p>
        </div>
        <div class="circle_2">
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xxl-9 col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                <div class="inner_area">
                    <div class="header">
                        <h2>Lorem ipsum dolor sit amet</h2> 
                        <p>But I must explain to you how all this mistaken idea of 
                            denouncing pleasure and praising pain was born and I will 
                            give you a complete account of the system, and expound the actual
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Clients_section_end ==== -->




<!-- ==== footer === -->
<?php include('common/footer.php') ?>
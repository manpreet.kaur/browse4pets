<!-- ==== Header === -->
<?php include('common/header.php') ?>

<section class="login_section top-space dl ad">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mx-auto">
                <div class="login_wrap">
                    <div class="login_section_area">
                        <div class="header_area">
                            <h1>Checkout</h1>
                            <div class="header_image1">
                                <img src="images/vector1.png" alt="..." />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xxl-10 col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12 mx-auto">
                                <div class="plan_box">
                                    <div class="inner_area">
                                        <div class="header">
                                            <h2>Subscription Plan</h2>
                                        </div>
                                        <div class="content">
                                            <div class="left">
                                                <h6>Annual Subscription</h6>
                                                <div class="circle">
                                                </div>
                                            </div>
                                            <div class="right">
                                                <h5>£50.00</h5>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <div class="left">
                                                <h6>Add Price</h6>
                                                <p>Duration 30 days</p>
                                                <div class="circle">
                                                </div>
                                            </div>
                                            <div class="right">
                                                <h5>£15.00</h5>
                                            </div>
                                        </div>
                                        <div class="total">
                                            <div class="left">
                                                <p>Total</p>
                                            </div>
                                            <div class="right">
                                                <p>£65.00</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="payment_box">
                                    <div class="inner_area">
                                        <div class="header">
                                            <p>Payment options</p>
                                        </div>
                                        <div class="content">
                                            <p>Credit card</p>
                                            <form>
                                                <div class="form-group">
                                                    <label for="name">Name</label>
                                                    <input type="text" class="form-control" placeholder="Enter your name">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="image_4">
        <img src="images/vector2.png" alt="..." />
    </div>
    <div class="image_5">
        <img src="images/vector3.png" alt="..." />
    </div>
</section>


<!-- ==== footer === -->
<?php include('common/footer.php') ?>
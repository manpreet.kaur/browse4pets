<!-- ==== Header === -->
<?php include('common/header2.php') ?>

<section class="dashboard_section">
    <div class="dashboard_inner top-space">
        <?php include('dashboard_sidebar2.php') ?>
        <div class="right_side_wrap">
            <div class="setting">
                <div class="breadcame margin">
                    <div class="breadcame_area">
                        <h2>My Favourites</h2>
                    </div> 
                </div>                   
                <div class="listing_section favourites">
                    <div class="row">
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="highlights_wrap">
                                <div class="list_divide_main">
                                    <div class="list_divide item_padding">
                                        <div class="row">
                                            <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="item">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/listdog.png" alt="..." />
                                                            <div class="favt heart_color">
                                                                <a href="#">
                                                                    <i class="fas fa-heart"></i>
                                                                </a>
                                                            </div>
                                                            <div class="flag">
                                                                <img src="images/flag2.png" alt="..."/>
                                                            </div>
                                                            <div class="corner_title">
                                                                <p>Sold</p>
                                                            </div>
                                                        </div>
                                                        <div class="detail">
                                                                <div class="name">
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                                <div class="right">
                                                                    <i class="far fa-mars"></i>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                                <h6>
                                                                    £800.00
                                                                </h6>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="wfwefwfwea">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="item">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/listdog5.png" alt="..." />
                                                            <div class="favt heart_color">
                                                                <a href="#">
                                                                    <i class="fas fa-heart"></i>
                                                                </a>
                                                            </div>
                                                            <div class="flag">
                                                                <img src="images/flag1.png" alt="..."/>
                                                            </div>
                                                            <div class="corner_title change_bg1">
                                                                <p>On Hold</p>
                                                            </div>
                                                        </div>
                                                        <div class="detail">
                                                                <div class="name">
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                                <div class="right">
                                                                    <i class="far fa-mars"></i>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                                <h6>
                                                                    £800.00
                                                                </h6>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="wfwefwfwea">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="item">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/listdog2.png" alt="..." />
                                                            <div class="favt heart_color">
                                                                <a href="#">
                                                                    <i class="fas fa-heart"></i>
                                                                </a>
                                                            </div>
                                                            <div class="flag">
                                                                <img src="images/flag1.png" alt="..."/>
                                                            </div>
                                                            <div class="corner_title change_bg2">
                                                                <p>Available</p>
                                                            </div>
                                                        </div>
                                                        <div class="detail">
                                                                <div class="name">
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                                <div class="right">
                                                                    <i class="far fa-mars"></i>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                                <h6>
                                                                    £800.00
                                                                </h6>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="wfwefwfwea">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="item">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/listdog8.png" alt="..." />
                                                            <div class="favt heart_color">
                                                                <a href="#">
                                                                    <i class="fas fa-heart"></i>
                                                                </a>
                                                            </div>
                                                            <div class="flag">
                                                                <img src="images/flag2.png" alt="..."/>
                                                            </div>
                                                            <div class="corner_title">
                                                                <p>Sold</p>
                                                            </div>
                                                        </div>
                                                        <div class="detail">
                                                                <div class="name">
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                                <div class="right">
                                                                    <i class="far fa-venus-mars"></i>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                                <h6>
                                                                    £800.00
                                                                </h6>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="wfwefwfwea">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="item">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/listdog4.png" alt="..." />
                                                            <div class="favt heart_color">
                                                                <a href="#">
                                                                    <i class="fas fa-heart"></i>
                                                                </a>
                                                            </div>
                                                            <div class="flag">
                                                                <img src="images/flag2.png" alt="..."/>
                                                            </div>
                                                            <div class="corner_title change_bg2">
                                                                <p>Available</p>
                                                            </div>
                                                        </div>
                                                        <div class="detail">
                                                                <div class="name">
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                                <div class="right">
                                                                    <i class="far fa-venus"></i>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                                <h6>
                                                                    £800.00
                                                                </h6>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="wfwefwfwea">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="item">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/listdog6.png" alt="..." />
                                                            <div class="favt heart_color">
                                                                <a href="#">
                                                                    <i class="fas fa-heart"></i>
                                                                </a>
                                                            </div>
                                                            <div class="flag">
                                                                <img src="images/flag1.png" alt="..."/>
                                                            </div>
                                                            <div class="corner_title change_bg2">
                                                                <p>Available</p>
                                                            </div>
                                                        </div>
                                                        <div class="detail">
                                                                <div class="name">
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                                <div class="right">
                                                                    <i class="far fa-mars"></i>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                                <h6>
                                                                    £800.00
                                                                </h6>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="wfwefwfwea">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ==== Footer ==== -->
<?php include('common/footer_2.php') ?>

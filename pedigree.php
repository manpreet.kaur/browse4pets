<!-- ==== Header === -->
<?php include('common/header.php') ?>

<section class="submit_listing_section top-space">
    <div class="container">
        <div class="row">
            <div class="col-xxl-10 col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12 mx-auto">
                <div class="submit_listing_area">
                    <div class="header_area">
                        <h1>Submit Your Listing</h1>
                        <p>Please fill in the fields below to post your classified ad.
                        </p>
                        <div class="header_image1">
                            <img src="images/vector1.png" alt="..." />
                        </div>
                    </div>
                    <form>
                        <div class="form_step4">
                            <div class="box_area">
                                <div class="top_bar">
                                    <div class="line_bar">
                                        <div class="bar_inn" style="width:75%;"></div>
                                        <div class="bar_wrap">
                                            <div class="bar_circle circle_1"></div>
                                            <div class="bar_circle circle_2"></div>
                                            <div class="bar_circle circle_3"></div>
                                            <div class="bar_circle circle_4"></div>
                                            <div class="bar_circle circle_5"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="top_bar_name">
                                    <div class="bar_circle circle_1">Details</div>
                                    <div class="bar_circle circle_2">Photos</div>
                                    <div class="bar_circle circle_3">Health Tests</div>
                                    <div class="bar_circle circle_4">Pedigree</div>
                                    <div class="bar_circle circle_5">Plans</div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="pedigree_area">
                                            <div class="head">
                                                <h6>
                                                    Pedigree
                                                </h6>
                                                <p>
                                                    Lorem ipsum dolor et, consectetur adipiscing elit. Lorem ipsum dolor amet, adipiscing elit. Lo sit amet, consectetur adipiscing elit.
                                                </p>
                                            </div>
                                           <div class="pedigree_inner_area">
                                                <div class="one_third form_first_boxes">
                                                    <div class="name">
                                                        <p>
                                                            Parents
                                                        </p>
                                                    </div>
                                                    <div class="list_area">
                                                        <div class="list_inner_side">
                                                            <div class="gread_grand_parent">
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Sire">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox" checked>
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Dam">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault2">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="one_third form_second_boxes">
                                                    <div class="name">
                                                        <p>
                                                            Grandparents
                                                        </p>
                                                    </div>
                                                    <div class="list_area">
                                                        <div class="list_inner_side">
                                                            <div class="gread_grand_parent">
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Sire">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault3">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Dam">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault4">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="gread_grand_parent">
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Sire">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault5">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Dam">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault5">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="one_third form_third_boxes">
                                                    <div class="name">
                                                        <p>
                                                            Great Grandparents
                                                        </p>
                                                    </div>
                                                    <div class="list_area">
                                                        <div class="list_inner_side">
                                                            <div class="gread_grand_parent">
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Sire">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault7">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Dam">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault8">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="gread_grand_parent">
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Sire">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault9">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Dam">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault10">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="gread_grand_parent">
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Sire">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefaul11">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Dam">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault12">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="gread_grand_parent">
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Sire">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault13">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Dam">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault14">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="one_third form_forth_boxes">
                                                    <div class="name">
                                                        <p>
                                                            Great Great Grandparents
                                                        </p>
                                                    </div>
                                                    <div class="list_area">
                                                        <div class="list_inner_side">
                                                            <div class="gread_grand_parent">
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Sire">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault15">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Dam">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault16">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="gread_grand_parent">
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Sire">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault17">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Dam">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault18">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="gread_grand_parent">
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Sire">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault19">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Dam">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault20">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="gread_grand_parent">
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Sire">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault21">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Dam">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefaul22">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="gread_grand_parent">
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Sire">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault23">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Dam">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault24">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="gread_grand_parent">
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Sire">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault25">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Dam">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault26">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="gread_grand_parent">
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Sire">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault27">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Dam">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault28">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="gread_grand_parent">
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Sire">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault29">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="outer_group">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" placeholder="Dam">
                                                                    </div>
                                                                    <div class="form-check form-switch">
                                                                        <label class="form-check-label" for="flexSwitchCheckDefault30">Is a Champion</label>
                                                                        <label class="switch">
                                                                            <input type="checkbox">
                                                                            <span class="slider"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                           </div> 
                                        </div>
                                        <div class="generation_type_wrap">
                                            <div class="generation_type">
                                                <div class="custom-control-radio custom-radio ps-3">
                                                    <input class="form-check-input" type="radio" name="where" id="r-1" value="1" checked="">
                                                    <label class="form-check-label" for="r-1">
                                                    Display 4 generations in ad 
                                                    </label>
                                                </div>
                                                <div class="custom-control-radio custom-radio ps-3">
                                                    <input class="form-check-input" type="radio" name="where" id="r-2" value="2">
                                                    <label class="form-check-label" for="r-2">
                                                    Display 3 generations in ad 
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="button_area">
                                            <div class="left">
                                                <ul>
                                                    <li>
                                                        <a href="health_tests.php" class="btn btn-primary back"><i class="fal fa-long-arrow-left pe-2"></i>Back</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;" class="btn btn-primary back">Save Draft</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="right">
                                                <a href="plan.php" class="btn btn-primary next_steps_4">Continue</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="image_2 d-lg-block d-none">
        <img src="images/vector2.png" alt="..." />
    </div>
    <div class="image_3 d-lg-block d-none">
        <img src="images/vector3.png" alt="..." />
    </div>
</section>





<!-- ==== footer === -->
<?php include('common/footer.php') ?>
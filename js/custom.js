$(window).scroll(function(){
    var scroll = $(window).scrollTop();
    if(scroll >= 20)
    {
        $(".header-main-section").addClass("shadow");
    }
    else
    {
        $(".header-main-section").removeClass("shadow");
    }

    if(scroll > 0 || scroll == 0)
    {
        $('.main_banner_dots_container li').removeClass('active');
        $('.main_banner_dots_container li.first_element').addClass('active');
    }

    if(scroll > 1045)
    {
        $('.main_banner_dots_container li').removeClass('active');
        //$('.main_banner_dots_container li.second_element').addClass('active');
    }

    if(scroll > 1798)
    {
        $('.main_banner_dots_container li').removeClass('active');
        //$('.main_banner_dots_container li.third_element').addClass('active');
    }

    if(scroll > 2900)
    {
        $('.main_banner_dots_container li').removeClass('active');
        $('.main_banner_dots_container li.fourth_element').addClass('active');
    }

    if(scroll > 3740)
    {
        $('.main_banner_dots_container li').removeClass('active');
        //$('.main_banner_dots_container li.fifth_element').addClass('active');
    }

    console.log(scroll);
});


// Open Close resposnive menu
/* Open menu */
if($('.res_menubar').length)
{
    $('body').on('click', '.res_menubar', function(){
        $(this).parents('.responsive_menu').find('.bar').addClass('hide_bar');
        $(this).parents('.responsive_menu').find('.open_menu').addClass('show_menu');
        $('body').addClass('scroll_off');
    });
}
// Open Close resposnive menu

/* Open menu */
if($('.res_menubar').length)
{
    $('body').on('click', '.res_menubar', function(){
        $(this).parents('.responsive_menu').find('.open_menu').addClass('show_open');
        $('body').addClass('scroll_off');
    });
}

/* Close menu */
if($('.cross_menu a').length)
{
    $('body').on('click', '.cross_menu a', function(){
        $(this).parents('.responsive_menu').find('.open_menu.show_open').removeClass('show_open');
        $('body').removeClass('scroll_off');
    });
}


    $('body').on('click', '.open_filter', function(){
        $(this).parents('.listing_sections').find('.sidebar_filters_area').addClass('dropdown');
        $('body').addClass('scroll_off');
    });

    $('body').on('click', '.cross_filter', function(){
        $(this).parents('.listing_sections').find('.sidebar_filters_area').removeClass('dropdown');
        $('body').removeClass('scroll_off');
    });


// Height match columns
$(document).ready(function () {
    $('.match_height_col').matchHeight();
    $('.match_height_txt').matchHeight();
});

if($('#listing').length)
{
    $('#listing').slick({
        rows:2,
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 5,
        autoplay: false,
        autoplaySpeed: 2000,
        arrows: true, 
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    rows:1,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    rows:1,
                }
            }
        ]
    });
}

if($('#safte').length)
{
    $('#safte').owlCarousel({
        center: true,
        stagePadding: 600,
        items:1,
        dots:false,
        loop:true,
        margin:80,
        nav: true,
        responsive:{
            0:{
                items:1,
                nav:false,
                stagePadding: 10,
            },
            576:{
                items:1,
                nav:false,
                stagePadding: 100,
            },
            768:{
                items:1,
                nav:false,
                margin:30,
                stagePadding: 150,
            },
            900:{
                items:1,
                nav:false,
                margin:50,
                stagePadding: 200,
            },
            1024:{
                items:1,
                nav:false,
                stagePadding: 200,
            },
            1200:{
                items:1,
                nav:false,
                stagePadding: 400,
            },
            1400:{
                items:1,
                stagePadding: 400,
            },
            1600:{
                items:1,
            },
            2880:{
                items:1,
                stagePadding: 950,
            }
        }
    });
}


/*function pagination() {
  var offset = $(document).scrollTop(),
        windowHeight = $(window).height(),
        body = $('body');
        console.log("height",windowHeight * 2.75)
    switch (true)
    {
        case (offset > ((windowHeight * 3.75))):
            body.removeClass().addClass('page-5');
        break;
        
        case (offset > (windowHeight * 2.75)):
            body.removeClass().addClass('page-4');
        break;
        
        case (offset > (windowHeight * 1.75)):
            body.removeClass().addClass('page-3');
        break;
        
        case (offset > (windowHeight * .75)):
            body.removeClass().addClass('page-2');
        break;
        
        case (offset < windowHeight):
            body.removeClass().addClass('page-1');
        break;
        
        default:
body.removeClass().addClass('page-1');
   }
}

pagination();*/

/*$(document).scroll(function() {
    id = $('section').attr('id');
    alert(id);
});*/

/*$('.main_banner_dots_container .first_element a').on('click', function(){
    $('html, body').animate({
        scrollTop: $('#page4').offset().top - 300
    }, 0);
});*/



// Scrolls to the selected menu item on the page
$(function() {
    $('.main_banner_dots_container ul li a').click(function() {
        id = $(this).attr('href');
        if(location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname)
        {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            
            scrollTopHeight = 0;
            if(id == '#page4')
            {
                scrollTopHeight = 300;
            }

            if(target.length)
            {
                targetId = target[0].id;
                if(targetId)
                {
                    $('.main_banner_dots_container ul li').removeClass('active');
                    $("a[href='#"+targetId+"']").parent().addClass('active');
                }

                $('html,body').animate({
                    scrollTop: target.offset().top - (scrollTopHeight && scrollTopHeight > 0 ? scrollTopHeight : 120)
                }, 0);
                return false;
            }
        }
    });
});



$( function() {
    $( ".budget-range" ).slider({
        range: true,
        min: 1000,
        max: 50000,
        values: [ 5000, 45000 ],
        slide: function( event, ui ) {
            $( ".budget-min-value" ).text('$' + ui.values[ 0 ] );
            $( ".budget-max-value" ).text('$' + ui.values[ 1 ] );
        }
    });
    $( ".budget-min-value" ).text('$' + $( ".budget-range" ).slider( "values", 0 ));
    $( ".budget-max-value" ).text('$' + $( ".budget-range" ).slider( "values", 1 ));
} );


// Select 2
$('.select2').select2();




// $('body').on('click', '.next_steps_3', function(){
//     that = $(this);
//     parent = that.parents('.submit_listing_area');
//     parent.find('.form_step3').addClass('d-none');
//     parent.find('.form_step4').removeClass('d-none');
// }); 
// $('body').on('click', '.next_steps_4', function(){
//     that = $(this);
//     parent = that.parents('.submit_listing_area');
//     parent.find('.form_step4').addClass('d-none');
//     parent.find('.form_step5').removeClass('d-none');
// });   

   


// $('body').on('click', '.back_steps_1', function(){
//     that = $(this);
//     parent = that.parents('.submit_listing_area');
//     parent.find('.form_step2').addClass('d-none');
//     parent.find('.form_step1').removeClass('d-none');
// }); 

// $('body').on('click', '.back_steps_2', function(){
//     that = $(this);
//     parent = that.parents('.submit_listing_area');
//     parent.find('.form_step3').addClass('d-none');
//     parent.find('.form_step2').removeClass('d-none');
// });

// $('body').on('click', '.back_steps_3', function(){
//     that = $(this);
//     parent = that.parents('.submit_listing_area');
//     parent.find('.form_step4').addClass('d-none');
//     parent.find('.form_step3').removeClass('d-none');
// });

$('body').on('click', '.has-submenu', function(){
    that = $(this);
    parent = that.parents('.inner_res_menu');
    parent.find('.submenu').toggle('collapse');
});

// Start Date
$('#start_date').datepicker({
    autoclose: false,
    container: '#start_date_box',
    todayHighlight: true,
    orientation: 'auto',
    //endDate: "dateToday",
    format: 'dd/mm/yyyy'
});

$('#new_date').datepicker({
    autoclose: false,
    container: '#new_date_box',
    todayHighlight: true,
    orientation: 'auto',
    //endDate: "dateToday",
    format: 'dd/mm/yyyy'
});

// End Date
$('#end_date').datepicker({
    autoclose: false,
    container: '#end_date_box',
    todayHighlight: true,
    orientation: 'auto',
    //endDate: "dateToday",
    format: 'dd/mm/yyyy'
});



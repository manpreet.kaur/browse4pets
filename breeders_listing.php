<!-- ==== Header === -->
<?php include('common/header.php') ?>

<section class="listing_sections top-space">
    <div class="listing_wrap">
        <div class="left_area">
            <div class="side_bar">
                <div class="sidebar_rows_area">
                    <div class="sidebar_filters_area">
                        <div class="top_heading d-md-none d-block">
                            <a href="javascript:;" class="open_filter">
                                <h5>Filters</h5>
                                <i class="fas fa-filter"></i>
                            </a>
                        </div>
                        <div class="inner_filters_area">
                            <div class="top_heading">
                                    <h2>Filters</h2>
                                    <a href="javascript:;" class="open_filter">
                                    </a>
                            </div>
                            <div class="locat_drop">
                                <form action="">
                                    <div class="form_group">
                                        <select class="form-select" aria-label="Default select example">
                                            <option selected>Location</option>
                                            <option value="1">...</option>
                                            <option value="2">...</option>
                                            <option value="3">...</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                            <div class="scroable_area">
                                <!-- <div class="top_heading d-none">
                                    <h2>Filters</h2>
                                    <a href="javascript:;" class="open_filter">
                                    </a>
                                </div> -->
                                <div class="accordion" id="accordionExample">
                                    <div class="cross_row">
                                        <a href="javascript:;" class="cross_filter">
                                            <img src="images/cross.png" alt="..." />
                                        </a>
                                    </div>
                                    <div class="accordion-item">
                                        <div class="filter_box district_box">
                                            <h2 class="accordion-header" id="headingfive">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                                                    <div class="filter_title">
                                                        <h5>Breed</h5>
                                                    </div>
                                                </button>
                                            </h2>
                                            <div id="collapsefive" class="accordion-collapse collapse" aria-labelledby="headingfive" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <div class="search_box">
                                                        <form action="" method="get">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="search" placeholder="Search"/>
                                                                <button type="button" class="btn btn-default">
                                                                    <i class="far fa-search"></i>
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="check_row">
                                                        <ul>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="4-1" name="accessibility[]" value="1">
                                                                    <label class="custom-control-label" for="4-1">Akits</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="4-2" name="accessibility[]" value="2">
                                                                    <label class="custom-control-label" for="4-2">Basenji</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="4-3" name="accessibility[]" value="3">
                                                                    <label class="custom-control-label" for="4-3">Bulldog</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="4-4" name="accessibility[]" value="4" checked>
                                                                    <label class="custom-control-label" for="4-4">Bearded Collie</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="4-5" name="accessibility[]" value="5">
                                                                    <label class="custom-control-label" for="4-5">Cairn Terrier</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="4-6" name="accessibility[]" value="6">
                                                                    <label class="custom-control-label" for="4-6">Dalmatin</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="4-7" name="accessibility[]" value="7">
                                                                    <label class="custom-control-label" for="4-7">English Setter</label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="accordion-item">
                                        <div class="filter_box district_box">
                                            <h2 class="accordion-header" id="headingsix">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsesix" aria-expanded="false" aria-controls="collapsesix">
                                                    <div class="filter_title">
                                                        <h5>Color</h5>
                                                    </div>
                                                </button>
                                            </h2>
                                            <div id="collapsesix" class="accordion-collapse collapse" aria-labelledby="headingsix" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <div class="check_row">
                                                        <ul>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="5-1" name="accessibility[]" value="1">
                                                                    <label class="custom-control-label" for="5-1">Black</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="5-2" name="accessibility[]" value="2">
                                                                    <label class="custom-control-label" for="5-2">White</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="5-3" name="accessibility[]" value="3">
                                                                    <label class="custom-control-label" for="5-3">Brown</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="5-4" name="accessibility[]" value="5" checked>
                                                                    <label class="custom-control-label" for="5-4">Golden</label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <div class="filter_box district_box">
                                            <h2 class="accordion-header" id="headingseven">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseseven" aria-expanded="false" aria-controls="collapseseven">
                                                    <div class="filter_title">
                                                        <h5>Gender</h5>
                                                    </div>
                                                </button>
                                            </h2>
                                            <div id="collapseseven" class="accordion-collapse collapse" aria-labelledby="headingseven" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <div class="check_row">
                                                        <ul>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="6-1" name="accessibility[]" value="1">
                                                                    <label class="custom-control-label" for="6-1">Male</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="6-2" name="accessibility[]" value="2">
                                                                    <label class="custom-control-label" for="6-2">Female</label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <div class="filter_box district_box">
                                            <h2 class="accordion-header" id="headingeight">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseeight" aria-expanded="false" aria-controls="collapseeight">
                                                    <div class="filter_title">
                                                        <h5>Age</h5>
                                                    </div>
                                                </button>
                                            </h2>
                                            <div id="collapseeight" class="accordion-collapse collapse" aria-labelledby="headingeight" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <div class="check_row">
                                                        <ul>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="7-1" name="accessibility[]" value="1">
                                                                    <label class="custom-control-label" for="7-1">1 month</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="7-2" name="accessibility[]" value="2">
                                                                    <label class="custom-control-label" for="7-2">1 month</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="7-3" name="accessibility[]" value="3">
                                                                    <label class="custom-control-label" for="7-3">1 month</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="7-4" name="accessibility[]" value="4" checked>
                                                                    <label class="custom-control-label" for="7-4">1 month</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="7-5" name="accessibility[]" value="5">
                                                                    <label class="custom-control-label" for="7-5">1 month</label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <div class="filter_box district_box">
                                            <h2 class="accordion-header" id="headingnine">
                                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsenine" aria-expanded="false" aria-controls="collapsenine">
                                                    <div class="filter_title">
                                                        <h5>Price range</h5>
                                                    </div>
                                                </button>
                                            </h2>
                                            <div id="collapsenine" class="accordion-collapse collapse" aria-labelledby="headingnine" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    <div class="price_range">
                                                        <p>
                                                                <span class="min-value budget-min-text"></span>
                                                            <span class="max-value budget-max-text"></span>
                                                            <input type="hidden" class="budget-min-value" name="budget_min_value" value="100">
                                                            <input type="hidden" class="budget-max-value" name="budget_max_value" value="10000">
                                                        </p>
                                                        <div class="gz-range budget-range"></div>
                                                        <div class="point">
                                                            <p>0</p>
                                                            <p>
                                                                100
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
        <div class="right_area">
          <div class="right_inner_box">
                <div class="search_box">
                    <form action="" method="get">
                        <div class="form-group">
                            <input type="text" class="form-control" name="search" placeholder="What are you looking for?"/>
                            <button type="button" class="btn btn-default">
                                <i class="far fa-search"></i>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="top_heading d-md-none d-block">
                    <a href="javascript:;" class="open_filter">
                        <h5>Filters<i class="fas fa-filter ps-2"></i></h5>
                    </a>
                </div>
                <div class="listing_area">
                    <div class="tab_area">
                        <div class="left_in">
                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">All</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Pedigree</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Other</button>
                                </li>
                            </ul>
                        </div>
                        <!-- <div class="right_in">
                            <div class="dropdown">
                                <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                    Sort by
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <li><a class="dropdown-item" href="#">Name</a></li>
                                    <li><a class="dropdown-item" href="#">Amount</a></li>
                                    <li><a class="dropdown-item" href="#">City</a></li>
                                </ul>
                            </div>
                        </div> -->
                    </div>
                    <!-- <div class=""> -->
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="highlights_section listing_highlights" id="page2">
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="heading">
                                            <h2>
                                                Highlights
                                            </h2>
                                        </div>
                                        <div class="highlights_wrap">
                                            <div class="row">
                                                <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/dog.png" alt="..." />
                                                        </div>
                                                        <div class="detail">
                                                            <div class="name">
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="#">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="favt">
                                                            <a href="#">
                                                                <i class="fal fa-heart"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/dog2.png" alt="..." />
                                                        </div>
                                                        <div class="detail">
                                                            <div class="name">
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="#">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="favt">
                                                            <a href="#">
                                                                <i class="fal fa-heart"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/dog3.png" alt="..." />
                                                        </div>
                                                        <div class="detail">
                                                            <div class="name">
                                                                <div class="flag">
                                                                    <img src="images/flag2.png" alt="..." />
                                                                </div>
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="#">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="favt">
                                                            <a href="#">
                                                                <i class="fal fa-heart"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/dog2.png" alt="..." />
                                                        </div>
                                                        <div class="detail">
                                                            <div class="name">
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="#">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="favt">
                                                            <a href="#">
                                                                <i class="fal fa-heart"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="listing_section breeders_listing_section  search_result">
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="heading">
                                            <h2>
                                                Search results
                                            </h2>
                                        </div>
                                        <div class="highlights_wrap">
                                            <div class="list_divide_main">
                                                <div class="list_divide">
                                                    <div class="breeders_slick">
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog5.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog3.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog2.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag1.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog4.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog5.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag1.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog2.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog4.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog3.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="highlights_section listing_highlights" id="page2">
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="heading">
                                            <h2>
                                                Highlights
                                            </h2>
                                        </div>
                                        <div class="highlights_wrap">
                                            <div class="row">
                                                <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/dog.png" alt="..." />
                                                        </div>
                                                        <div class="detail">
                                                            <div class="name">
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="#">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="favt">
                                                            <a href="#">
                                                                <i class="fal fa-heart"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/dog2.png" alt="..." />
                                                        </div>
                                                        <div class="detail">
                                                            <div class="name">
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="#">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="favt">
                                                            <a href="#">
                                                                <i class="fal fa-heart"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/dog3.png" alt="..." />
                                                        </div>
                                                        <div class="detail">
                                                            <div class="name">
                                                                <div class="flag">
                                                                    <img src="images/flag2.png" alt="..." />
                                                                </div>
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="#">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="favt">
                                                            <a href="#">
                                                                <i class="fal fa-heart"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/dog2.png" alt="..." />
                                                        </div>
                                                        <div class="detail">
                                                            <div class="name">
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="#">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="favt">
                                                            <a href="#">
                                                                <i class="fal fa-heart"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="listing_section breeders_listing_section  search_result">
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="heading">
                                            <h2>
                                                Search results
                                            </h2>
                                        </div>
                                        <div class="highlights_wrap">
                                            <div class="list_divide_main">
                                                <div class="list_divide">
                                                    <div class="breeders_slick">
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog5.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog3.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog2.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag1.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog4.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog5.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag1.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog2.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog4.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog3.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="highlights_section listing_highlights" id="page2">
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="heading">
                                            <h2>
                                                Highlights
                                            </h2>
                                        </div>
                                        <div class="highlights_wrap">
                                            <div class="row">
                                                <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/dog.png" alt="..." />
                                                        </div>
                                                        <div class="detail">
                                                            <div class="name">
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="#">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="favt">
                                                            <a href="#">
                                                                <i class="fal fa-heart"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/dog2.png" alt="..." />
                                                        </div>
                                                        <div class="detail">
                                                            <div class="name">
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="#">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="favt">
                                                            <a href="#">
                                                                <i class="fal fa-heart"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/dog3.png" alt="..." />
                                                        </div>
                                                        <div class="detail">
                                                            <div class="name">
                                                                <div class="flag">
                                                                    <img src="images/flag2.png" alt="..." />
                                                                </div>
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="#">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="favt">
                                                            <a href="#">
                                                                <i class="fal fa-heart"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                                    <div class="one_third">
                                                        <div class="img_area">
                                                            <img src="images/dog2.png" alt="..." />
                                                        </div>
                                                        <div class="detail">
                                                            <div class="name">
                                                                <div class="flag">
                                                                    <img src="images/flag1.png" alt="..." />
                                                                </div>
                                                                <div class="left">
                                                                    <p>
                                                                        Bichon Frise Golden 
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="loaction">
                                                                <span>
                                                                    Dublin, Ireland
                                                                </span>
                                                            </div>
                                                            <div class="price">
                                                                <div class="visite">
                                                                    <a href="#">
                                                                        <i class="fal fa-long-arrow-up"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="favt">
                                                            <a href="#">
                                                                <i class="fal fa-heart"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="listing_section breeders_listing_section  search_result">
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="heading">
                                            <h2>
                                                Search results
                                            </h2>
                                        </div>
                                        <div class="highlights_wrap">
                                            <div class="list_divide_main">
                                                <div class="list_divide">
                                                    <div class="breeders_slick">
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog5.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog3.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog2.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag1.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog4.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog5.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag1.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog2.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog4.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="one_third match_height_col">
                                                                <div class="img_area">
                                                                    <img src="images/listdog3.png" alt="..." />
                                                                    <div class="favt">
                                                                        <a href="#">
                                                                            <i class="fal fa-heart"></i>
                                                                        </a>
                                                                    </div>
                                                                    <div class="flag">
                                                                        <img src="images/flag2.png" alt="..."/>
                                                                    </div>
                                                                </div>
                                                                <div class="detail">
                                                                        <div class="name">
                                                                        <div class="left">
                                                                            <p>
                                                                                Bichon Frise Golden 
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="loaction">
                                                                        <span>
                                                                            Dublin, Ireland
                                                                        </span>
                                                                    </div>
                                                                    <div class="price">
                                                                        <div class="visite">
                                                                            <a href="#">
                                                                                <i class="fal fa-long-arrow-up"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<!-- ==== footer === -->
<?php include('common/footer.php') ?>
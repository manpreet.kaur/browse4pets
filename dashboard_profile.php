<!-- ==== Header === -->
<?php include('common/header3.php') ?>

<section class="dashboard_section">
    <div class="dashboard_inner">
    <?php include('dashboard_sidebar.php') ?>
        <div class="right_side_wrap top-space">
            <div class="setting">
                <form>
                    <div class="row">
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="breadcame">
                                <div class="breadcame_area">
                                    <h2>My Profile</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-7 col-xl-7 col-lg-7 col-md-10 col-sm-12 col-12 mx-auto">
                            <div class="change_password">
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="photo">
                                            <p>Photo</p>
                                        </div>
                                        <div class="image">
                                            <img src="images/profile.png" alt="image not found" />
                                            <div class="iconwrap">
                                                <a href="javascript:;">
                                                <i class="fal fa-pen"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="first_name">First Name</label>
                                            <input type="text" class="form-control" placeholder="Enter First Name" value="Dianne" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="last_name">Last Name</label>
                                            <input type="text" class="form-control" placeholder="Enter Last Name" value="Jane" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="email">Email Address</label>
                                            <input type="email" class="form-control" placeholder="Enter Email" value="diannejane@samplemail.com" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="phone_number">Telephone Number</label>
                                            <input type="text" class="form-control" placeholder="Enter Phone Number" value="+1 234 5678 9101" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="country">Country</label>
                                            <select class="form-select">
                                                <option>Select</option>
                                                <option value="">California</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                                <option value="">three</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="post_code">Postcode</label>
                                            <select class="form-select">
                                                <option>Select</option>
                                                <option value="">California</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                                <option value="">three</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="country">County</label>
                                            <select class="form-select">
                                                <option>Select</option>
                                                <option value="">California</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                                <option value="">three</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="city">City</label>
                                            <select class="form-select">
                                                <option>Select</option>
                                                <option value="">California</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                                <option value="">three</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="address">Address</label>
                                            <input type="text" class="form-control" placeholder="Address" value="16 st lorem ipsum" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="save_button">
                                            <a href="javascript:;" class="btn btn-primary-2">Save Changes</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- ==== Footer ==== -->
<?php include('common/footer_2.php') ?>

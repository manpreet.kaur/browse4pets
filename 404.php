<?php include('common/header.php') ?>

<section class="error_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="error_detail text-center">
                     <div class="img_area">
                        <img src="images/404_img.png" alt=".." />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="image_1 d-lg-block d-none">
        <img src="images/Vector4.png" alt="..." />
    </div>
    <div class="image_2 d-lg-block d-none">
        <img src="images/circle_img.png" alt="..." />
    </div>
</section>


<!-- ==== Header === -->
<?php include('common/footer.php') ?>
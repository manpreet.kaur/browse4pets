<!-- ==== Header === -->
<?php include('common/header.php') ?>

<section class="submit_listing_section top-space">
    <div class="container">
        <div class="row">
            <div class="col-xxl-10 col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12 mx-auto">
                <div class="submit_listing_area">
                    <div class="header_area">
                        <h1>Submit Your Listing</h1>
                        <p>Please fill in the fields below to post your classified ad.
                        </p>
                        <div class="header_image1">
                            <img src="images/vector1.png" alt="..." />
                        </div>
                    </div>
                    <form>
                        <div class="form_step5">
                            <div class="box_area">
                                <div class="top_bar">
                                    <div class="line_bar">
                                        <div class="bar_inn" style="width:100%;"></div>
                                        <div class="bar_wrap">
                                            <div class="bar_circle circle_1"></div>
                                            <div class="bar_circle circle_2"></div>
                                            <div class="bar_circle circle_3"></div>
                                            <div class="bar_circle circle_4"></div>
                                            <div class="bar_circle circle_5"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="top_bar_name">
                                    <div class="bar_circle circle_1">Details</div>
                                    <div class="bar_circle circle_2">Photos</div>
                                    <div class="bar_circle circle_3">Health Tests</div>
                                    <div class="bar_circle circle_4">Pedigree</div>
                                    <div class="bar_circle circle_5">Plans</div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="button_area">
                                            <div class="left">
                                                <a href="pedigree.php" class="btn btn-primary back back_steps_4">Back</a>
                                            </div>
                                            <div class="right">
                                                <a href="javascript:;" class="btn btn-primary next_steps_5">Continue</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="image_2 d-lg-block d-none">
        <img src="images/vector2.png" alt="..." />
    </div>
    <div class="image_3 d-lg-block d-none">
        <img src="images/vector3.png" alt="..." />
    </div>
</section>





<!-- ==== footer === -->
<?php include('common/footer.php') ?>
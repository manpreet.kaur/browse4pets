<!-- ==== Header === -->
<?php include('common/header2.php') ?>

<section class="dashboard_section">
    <div class="dashboard_inner">
    <?php include('dashboard_sidebar2.php') ?>
        <div class="right_side_wrap top-space">
            <div class="setting">
                <form>
                    <div class="row">
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="breadcame margin">
                                <div class="breadcame_area transaction_breadcame">
                                    <div class="left">
                                        <h2>Dog Health Test</h2>
                                    </div>
                                    <div class="right">
                                        <div class="search_input_with_btn">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Search">
                                                <div class="input-group-append">
                                                    <a href="javascript:;">
                                                        <i class="far fa-search"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="filter">
                                            <ul>
                                                <li class="dropdown actions_dropdown">
                                                    <a class="dropdown-toggle" id="actions" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-expanded="true">
                                                        <i class="fal fa-filter"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="actions" data-popper-placement="bottom-end">
                                                        <form action="">
                                                            <div class="row">
                                                                <!-- <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                    <div class="form-group">
                                                                        <label for="">
                                                                            Categories
                                                                        </label>
                                                                        <select id="inputcategory" class="form-select">
                                                                            <option selected="">.....</option>
                                                                            <option>....</option>
                                                                            <option>....</option>
                                                                            
                                                                        </select>
                                                                    </div>
                                                                </div> -->
                                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                    <div class="row">
                                                                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                            <label for="payment_type">
                                                                                Payment type
                                                                            </label>
                                                                        </div>
                                                                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                            <div class="form-group">
                                                                                <select class="form-select">
                                                                                    <option value="">Select</option>
                                                                                    <option value="">1</option>
                                                                                    <option value="">2</option>
                                                                                    <option value="">3</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                            <label for="date">
                                                                                Date
                                                                            </label>
                                                                        </div>
                                                                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                                            <div class="form-group">
                                                                                <div class="input-group" id="start_date_box">
                                                                                    <input type="text" class="form-control" name="start_date" id="start_date" placeholder="To">
                                                                                    <div class="icon">
                                                                                        <i class="fi fi-rr-calendar"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                                            <div class="form-group">
                                                                                <div class="input-group" id="new_date_box">
                                                                                    <input type="text" class="form-control" name="start_date" id="new_date" placeholder="From">
                                                                                    <div class="icon">
                                                                                        <i class="fi fi-rr-calendar"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                    <label for="status">
                                                                        Status
                                                                    </label>
                                                                </div>
                                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                    <div class="form-group">
                                                                        <div class="custom-control-radio custom-radio">
                                                                            <input class="form-check-input" type="radio" name="where" id="r-1" value="1" checked="">
                                                                            <label class="form-check-label" for="r-1">
                                                                            All
                                                                            </label>
                                                                            <input class="form-check-input" type="radio" name="where" id="r-2" value="1">
                                                                            <label class="form-check-label" for="r-2">
                                                                            Paid
                                                                            </label>
                                                                            <input class="form-check-input" type="radio" name="where" id="r-3" value="1">
                                                                            <label class="form-check-label" for="r-3">
                                                                            Fail
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                    <div class="footer_button float-start">
                                                                        <button type="submit" class="btn btn-primary-7">Reset all</button>
                                                                    </div>
                                                                    <div class="footer_button">
                                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mx-auto">
                            <div class="button">
                                <ul>
                                    <li>
                                        <a href="javascript:;" class="btn btn-primary">Print</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" class="btn btn-primary">Export</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="change_password">
                                <div class="transaction my_order">
                                    <div class="table_row_area">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                        <i class="fal fa-sort pe-2"></i>
                                                            Breed
                                                        </th>
                                                        <th>
                                                        <i class="fal fa-sort pe-2"></i>
                                                            KC registered name 
                                                        </th>
                                                        <th>
                                                        <i class="fal fa-sort pe-2"></i>
                                                            Test
                                                        </th>
                                                        <th>
                                                        <i class="fal fa-sort pe-2"></i>
                                                            Result
                                                        </th>
                                                        <th>
                                                        <i class="fal fa-sort pe-2"></i>
                                                            Date
                                                        </th>
                                                        <th>
                                                        <!-- <i class="fal fa-sort pe-2"></i> -->
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="abc">
                                                        <td>
                                                            <div class="breed_color">
                                                                <a href="javascript:;">Golden Retriever</a> 
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                256478987
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">    
                                                                Lorem
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                Sit ipsum
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                22/03/2022
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="icon">
                                                                <div class="dropdown">
                                                                    <button class="dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                        <a href="javascript:;" class="color"><i class="far fa-ellipsis-v"></i></a>
                                                                    </button>
                                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                        <li><a class="dropdown-item" href="#">View</a></li>
                                                                        <li><a class="dropdown-item" href="#">Delete</a></li>
                                                                        <li><a class="dropdown-item" href="#">Download</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </td>             
                                                    </tr>
                                                    <tr class="abc">
                                                        <td>
                                                            <div class="breed_color">
                                                                <a href="javascript:;">Golden Retriever</a> 
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                256478987
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">    
                                                                Lorem
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                Sit ipsum
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                22/03/2022
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="icon">
                                                                <div class="dropdown">
                                                                    <button class="dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                        <a href="javascript:;" class="color"><i class="far fa-ellipsis-v"></i></a>
                                                                    </button>
                                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                        <li><a class="dropdown-item" href="#">View</a></li>
                                                                        <li><a class="dropdown-item" href="#">Delete</a></li>
                                                                        <li><a class="dropdown-item" href="#">Download</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </td>             
                                                    </tr>
                                                    <tr class="abc">
                                                        <td>
                                                            <div class="breed_color">
                                                                <a href="javascript:;">Golden Retriever</a> 
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                256478987
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">    
                                                                Lorem
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                Sit ipsum
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                22/03/2022
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="icon">
                                                                <div class="dropdown">
                                                                    <button class="dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                        <a href="javascript:;" class="color"><i class="far fa-ellipsis-v"></i></a>
                                                                    </button>
                                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                        <li><a class="dropdown-item" href="#">View</a></li>
                                                                        <li><a class="dropdown-item" href="#">Delete</a></li>
                                                                        <li><a class="dropdown-item" href="#">Download</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </td>             
                                                    </tr>
                                                    <tr class="abc">
                                                        <td>
                                                            <div class="breed_color">
                                                                <a href="javascript:;">Golden Retriever</a> 
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                256478987
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">    
                                                                Lorem
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                Sit ipsum
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                22/03/2022
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="icon">
                                                                <div class="dropdown">
                                                                    <button class="dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                        <a href="javascript:;" class="color"><i class="far fa-ellipsis-v"></i></a>
                                                                    </button>
                                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                        <li><a class="dropdown-item" href="#">View</a></li>
                                                                        <li><a class="dropdown-item" href="#">Delete</a></li>
                                                                        <li><a class="dropdown-item" href="#">Download</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </td>             
                                                    </tr>
                                                    <tr class="abc">
                                                        <td>
                                                            <div class="breed_color">
                                                                <a href="javascript:;">Golden Retriever</a> 
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                256478987
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">    
                                                                Lorem
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                Sit ipsum
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                22/03/2022
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="icon">
                                                                <div class="dropdown">
                                                                    <button class="dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                        <a href="javascript:;" class="color"><i class="far fa-ellipsis-v"></i></a>
                                                                    </button>
                                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                        <li><a class="dropdown-item" href="#">View</a></li>
                                                                        <li><a class="dropdown-item" href="#">Delete</a></li>
                                                                        <li><a class="dropdown-item" href="#">Download</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </td>             
                                                    </tr>
                                                    <tr class="abc">
                                                        <td>
                                                            <div class="breed_color">
                                                                <a href="javascript:;">Golden Retriever</a> 
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                256478987
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">    
                                                                Lorem
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                Sit ipsum
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                22/03/2022
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="icon">
                                                                <div class="dropdown">
                                                                    <button class="dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                        <a href="javascript:;" class="color"><i class="far fa-ellipsis-v"></i></a>
                                                                    </button>
                                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                        <li><a class="dropdown-item" href="#">View</a></li>
                                                                        <li><a class="dropdown-item" href="#">Delete</a></li>
                                                                        <li><a class="dropdown-item" href="#">Download</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </td>             
                                                    </tr>
                                                    <tr class="abc">
                                                        <td>
                                                            <div class="breed_color">
                                                                <a href="javascript:;">Golden Retriever</a> 
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                256478987
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">    
                                                                Lorem
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                Sit ipsum
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                22/03/2022
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="icon">
                                                                <div class="dropdown">
                                                                    <button class="dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                        <a href="javascript:;" class="color"><i class="far fa-ellipsis-v"></i></a>
                                                                    </button>
                                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                        <li><a class="dropdown-item" href="#">View</a></li>
                                                                        <li><a class="dropdown-item" href="#">Delete</a></li>
                                                                        <li><a class="dropdown-item" href="#">Download</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </td>             
                                                    </tr>
                                                    <tr class="abc">
                                                        <td>
                                                            <div class="breed_color">
                                                                <a href="javascript:;">Golden Retriever</a> 
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                256478987
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">    
                                                                Lorem
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                Sit ipsum
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="light_color">
                                                                22/03/2022
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="icon">
                                                                <div class="dropdown">
                                                                    <button class="dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                                        <a href="javascript:;" class="color"><i class="far fa-ellipsis-v"></i></a>
                                                                    </button>
                                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                                        <li><a class="dropdown-item" href="#">View</a></li>
                                                                        <li><a class="dropdown-item" href="#">Delete</a></li>
                                                                        <li><a class="dropdown-item" href="#">Download</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </td>             
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- ==== Footer ==== -->
<?php include('common/footer_2.php') ?>

<!-- ==== Header === -->
<?php include('common/header.php') ?>

<section class="login_section top-space dl ad">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mx-auto">
                <div class="login_wrap">
                    <div class="login_section_area">
                        <div class="header_area">
                            <h1>Submit Your Listing</h1>
                            <p>Please fill in the fields below to post your classified ad.
                            </p>
                            <div class="header_image1">
                                <img src="images/vector1.png" alt="..." />
                            </div>
                        </div>
                        <div class="box_area el">
                             <div class="content text-center">
                                 <h4>
                                     Ad Type
                                 </h4>
                                 <div class="add">
                                     <a href="submit_listing.php" class="btn btn-primary w-100">Available Dogs</a>
                                 </div>
                                 <div class="add">
                                     <a href="submit_listing2.php" class="btn btn-primary-2 w-100">Upcoming Litter</a>
                                 </div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="image_4">
        <img src="images/vector2.png" alt="..." />
    </div>
    <div class="image_5">
        <img src="images/vector3.png" alt="..." />
    </div>
</section>

<section class="foot_forget_img el">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="foot_img_forget">
                    <div class="img_area">
                        <img src="images/yorkie-1.png" alt=".." />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ==== footer === -->
<?php include('common/footer.php') ?>
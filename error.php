<?php include('common/header.php') ?>

<section class="succuess_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="succes">
                    <div class="row">
                        <div class="col-xxl-5 col-xl-5 col-lg-5 col-md-6 col-sm-8 col-12 m-auto text-center"> 
                            <div class="succes_inner text-center">
                                <div class="sign">
                                    <i class="fas fa-exclamation-triangle"></i>
                                </div>
                                <div class="dspt">
                                    <h5>
                                        Error
                                    </h5>
                                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. At laudantium ullam repudiandae molest</p>
                                </div>
                                <a href="#" class="btn btn-primary-1"><i class="fal fa-long-arrow-left me-2"></i>Back to home</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- ==== Header === -->
<?php include('common/footer.php') ?>
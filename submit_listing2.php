<!-- ==== Header === -->
<?php include('common/header.php') ?>

<section class="submit_listing_section top-space">
    <div class="container">
        <div class="row">
            <div class="col-xxl-10 col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12 mx-auto">
                <div class="submit_listing_area">
                    <div class="header_area">
                        <h1>Submit Your Listing</h1>
                        <p>Please fill in the fields below to post your classified ad.
                        </p>
                        <div class="header_image1">
                            <img src="images/vector1.png" alt="..." />
                        </div>
                    </div>
                    <form>
                        <div class="form_step1">
                            <div class="box_area">
                                <div class="top_bar">
                                <div class="line_bar">
                                        <div class="bar_inn" style="width:0%;"></div>
                                        <div class="bar_wrap">
                                            <div class="bar_circle circle_1"></div>
                                            <div class="bar_circle circle_2"></div>
                                            <div class="bar_circle circle_3"></div>
                                            <div class="bar_circle circle_4"></div>
                                            <div class="bar_circle circle_5"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="top_bar_name">
                                    <div class="bar_circle circle_1">Details</div>
                                    <div class="bar_circle circle_2">Photos</div>
                                    <div class="bar_circle circle_3">Health Tests</div>
                                    <div class="bar_circle circle_4">Pedigree</div>
                                    <div class="bar_circle circle_5">Plans</div>
                                </div>
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="advert_heading">Advert heading</label>
                                            <input type="text" class="form-control" placeholder="Enter Ad Title (include Breed)" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="breed">Breed</label>
                                            <select class="form-select">
                                                <option>Select Breed</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="category">Category</label>
                                            <select class="form-select">
                                                <option>Select Category</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="origin_country">Country of Origin</label>
                                            <input type="text" class="form-control" placeholder="Enter Country of Origin" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h4>Expected Date of Birth</h4>
                                    </div>
                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6">
                                        <div class="form-group">
                                            <label for="day">Day</label>
                                            <select class="form-select">
                                                <option>Day</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6">
                                        <div class="form-group">
                                            <label for="month">Month</label>
                                            <select class="form-select">
                                                <option>Month</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-4 col-6">
                                        <div class="form-group">
                                            <label for="year">Year</label>
                                            <select class="form-select">
                                                <option>Year</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="locality">Locality</label>
                                            <input type="text" class="form-control" placeholder="Enter Locality" autocomplete="off" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="country">County</label>
                                            <select class="form-select">
                                                <option>Select County</option>
                                                <option value="">one</option>
                                                <option value="">two</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label for="description">Description</label>
                                            <textarea type="text" class="form-control" placeholder="Enter Description" autocomplete="off" ></textarea>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="content">
                                            <p>*The description is displayed in search results when people are searching for ads.* </p>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="contents">
                                            <div class="left">
                                                <div class="icon">
                                                    <i class="fal fa-exclamation-circle"></i>
                                                </div>
                                            </div>
                                            <div class="right">
                                                <p>
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras facilisis mollis porttitor. Quisque aliquet in purus ac iaculis. Donec tempor molestie neque a ornare. Ut a egestas velit, quis laoreet
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="button">
                                        <div class="left">
                                            <ul>
                                                <li>
                                                    <a href="ad_type.php" class="btn btn-primary back"><i class="fal fa-long-arrow-left pe-2"></i>Back</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;" class="btn btn-primary back">Save Draft</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="right">
                                            <a href="photo.php" class="btn btn-primary next_steps_1">Continue</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="image_2 d-xl-block d-none">
        <img src="images/vector2.png" alt="..." />
    </div>
    <div class="image_3 d-xl-block d-none">
        <img src="images/vector3.png" alt="..." />
    </div>
</section>





<!-- ==== footer === -->
<?php include('common/footer.php') ?>
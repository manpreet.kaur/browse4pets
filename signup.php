<?php include('common/header.php') ?>

<section class="signup_section top-space">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="header_area">
                    <h1>Sign up</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam maximus orci at turpis suscipit rutrum. 
                    </p>
                    <div class="header_image1">
                        <img src="images/vector1.png" alt="..." />
                    </div>
                </div>
            </div>
            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="signup_left_area">
                    <div class="left_formd">
                        <div class="row">
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label for="user_name">First name</label>
                                    <input type="text" class="form-control" placeholder="First name" autocomplete="off" />
                                </div>
                            </div>
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label for="user_name">Last name</label>
                                    <input type="text" class="form-control" placeholder="Last name" autocomplete="off" />
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="user_name">Buyer/seller Type</label>
                                    <select id="input" class="form-select">
                                        <option selected="">Private buyer or seller</option>
                                        <option>...</option>
                                        <option>...</option>
                                        <option>...</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="user_name">Business name/ Kennel name</label>
                                    <input type="text" class="form-control" placeholder="Enter name" />
                                </div>
                            </div>
                            <!-- <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="user_name">Seller ID</label>
                                    <select id="input" class="form-select">
                                        <option selected="">Seller ID</option>
                                        <option>...</option>
                                        <option>...</option>
                                        <option>...</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="user_name">RBE number</label>
                                    <input type="text" class="form-control" placeholder="RBE No" autocomplete="off" />
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="user_name">Maximum number of breeding bitches </label>
                                    <input type="text" class="form-control" placeholder="Max number of breeding bitches" autocomplete="off" />
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="user_name">Charitable number </label>
                                    <input type="text" class="form-control" placeholder="Charitable no" autocomplete="off" />
                                </div>
                            </div> -->
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label for="user_name">Phone number</label>
                                <div class="row">
                                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-4">
                                        <div class="form-group">
                                        <select id="input" class="form-select">
                                        <option selected="">44</option>
                                        <option>...</option>
                                        <option>...</option>
                                        <option>...</option>
                                    </select>
                                        </div>
                                    </div>
                                    <div class="col-xxl-9 col-xl-9 col-lg-9 col-md-9 col-sm-9 col-8">
                                        <div class="form-group">
                                            <input type="number" class="form-control" placeholder="Phone number" autocomplete="off" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="user_name">Email address</label>
                                    <input type="text" class="form-control" placeholder="Email address" autocomplete="off" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sign_wrap_img">
                        <img src="images/dfd.png" alt=".." />
                    </div>
                </div>
            </div>
            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="sign_up_form">
                    <div class="row">
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="user_name">Country</label>
                                <select id="input" class="form-select">
                                    <option selected="">Select</option>
                                    <option>...</option>
                                    <option>...</option>
                                    <option>...</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="user_name">Postcode</label>
                                <select id="input" class="form-select">
                                    <option selected="">Select</option>
                                    <option>...</option>
                                    <option>...</option>
                                    <option>...</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="user_name">County</label>
                                <select class="form-select select2" name="state_id">
                                    <option selected="">Select</option>
                                    <option>abc</option>
                                    <option>df</option>
                                    <option>gh</option>
                                </select>

                                <!-- <select id="input" class="form-select">
                                    <option selected="">Select</option>
                                    <option>...</option>
                                    <option>...</option>
                                    <option>...</option>
                                </select> -->
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="user_name">City</label>
                                <select id="input" class="form-select">
                                    <option selected="">Select</option>
                                    <option>...</option>
                                    <option>...</option>
                                    <option>...</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <label for="user_name">Address</label>
                                <input type="text" class="form-control" placeholder="Address" />
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" placeholder="Password" />
                                <div class="input-group-append">
                                    <a href="javascript:;"><i class="fas fa-eye"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="password">Confirm Password</label>
                                <input type="password" class="form-control" placeholder="Confirm password"  />
                                <div class="input-group-append">
                                    <a href="javascript:;"><i class="fas fa-eye"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="l-1" name="district[]" value="1" checked="">
                                    <label class="custom-control-label" for="l-1">This is to certify that I have read and agreed to browse4dogs Terms of service.</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="l-2" name="district[]" value="2" checked="">
                                    <label class="custom-control-label" for="l-2">I would like to subscribe to mailing list for offers & news</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="left">
                                <div class="icon">
                                    <i class="fal fa-exclamation-square"></i>
                                </div>
                            </div>
                            <div class="right">
                                <div class="content">
                                    <p>
                                    Once your account has been created, your Name, Phone Number, Seller ID and RBE Number (If applicable) cannot be changed. You will have to contact administrator if any changes are required.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="signup_btnn">
                                <a href="index.php" class="btn btn-primary w-100">Sign up</a>
                            </div>
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="sign_up_button">
                                <p>Already have an account? <a href="login.php">Login</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="image_2">
        <img src="images/vector2.png" alt="..." />
    </div>
</section>


<!-- ==== Header === -->
<?php include('common/footer.php') ?>
<div class="side_bar_section">
    <div class="side_bar_area sticky-top">
        <div class="box_area">
            <div class="image_area d-none d-md-block">
                <a href="javascript:;">
                    <div class="user-img-warp">
                        <div class="user_img">
                            <img src="images/profile.png" alt="..." />  
                        </div>
                   </div>
                </a>
                <div class="name">Dianne Jane</div>
            </div>
            <div class="menu_area">
                <ul class="d-none d-md-block">
                    <li class="active">
                        <a href="dashboard_profile.php">
                            <div class="icon"><i class="fal fa-user-alt"></i></div>
                            <div class="text">My Profile</div>
                        </a>
                    </li>
                    <li>
                        <a href="dashboard_favourites.php">
                            <div class="icon"><i class="fal fa-heart"></i></div>
                            <div class="text">My Favourites</div>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;">
                            <div class="icon"><i class="fal fa-comments-alt"></i></div>
                            <div class="text">Messages</div>
                        </a>
                    </li>
                    <li>
                        <a href="dashboard_change_password.php">
                            <div class="icon"><i class="fal fa-lock space2"></i></div>
                            <div class="text">Change Password</div>
                        </a>
                    </li>
                     <li>
                        <a href="javascript:;">
                            <div class="icon"><i class="fal fa-sign-out outer"></i></div>
                            <div class="text">Logout</div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
